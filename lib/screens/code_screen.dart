import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/screens/ride_options.dart';
import 'package:taxi_finder/widgets/numpad.dart';
import '../constant/colors.dart';

class CodeScreen extends StatefulWidget {
  const CodeScreen({Key? key}) : super(key: key);


  @override
  _CodeScreenState createState() => _CodeScreenState();
}

class _CodeScreenState extends State<CodeScreen> {
  final TextEditingController _codeController = TextEditingController();

  bool onClick = false;
  late String numCode;

  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: purple,
              child: Column(
                children: [
                  SizedBox(height: 0.15.sw),
                  Row(
                    children: [
                      IconButton(
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                          size: 24,
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ],
                  ),
                  SizedBox(height: 0.1.sw),
                  Text(
                    "عسلامة !",
                    style: GoogleFonts.cairo(
                      fontSize: 26.sp,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    textDirection: TextDirection.rtl,
                    textAlign: TextAlign.end,
                  ),
                  SizedBox(height: 0.05.sw),
                  Container(
                    height: 50,
                    width: 250,
                    margin: const EdgeInsets.symmetric(horizontal: 18.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                    ),
                    child: TextField(
                      textAlign: TextAlign.center,
                      controller: _codeController,
                      keyboardType: TextInputType.none,
                      enabled: false,
                      decoration: InputDecoration(
                        hintText: "Please enter your code",
                        hintStyle: TextStyle(
                          color: purple,
                          fontSize: 14.0.sp,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 0.05.sw),
                  Container(
                    height: 310.0.sp,
                    width: 300.0.sp,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    child: NumPad(
                      buttonSize: 50,
                      buttonColor: Colors.purple,
                      iconColor: Colors.deepPurple,
                      iconDeleteSize: 40,
                      controller: _codeController,
                      delete: () {
                        if (_codeController.text.isNotEmpty) {
                          _codeController.text = _codeController.text
                              .substring(0, _codeController.text.length - 1);
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 0.05.sw),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 120,
                        height: 45,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.yellow,
                              textStyle: const TextStyle(color: Colors.black),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(24.0))),
                          onPressed: ()  {

                            if (_codeController.text.isEmpty) {
                              displayToastMessage(
                                  "Entre votre code envoyer!", context);
                            } else {
                              setState(() {
                                onClick = true;
                              });
                              UserMethods.verifierCode(_codeController.text.toString(), context);
                              Future.delayed(
                                const Duration(seconds: 1,),
                                    () {
                                  setState(() {
                                    onClick = false;
                                  });
                                },
                              );

                            }
                          },
                          child: Container(
                            padding: EdgeInsets.fromLTRB(
                              0.01.sw,
                              0.01.sh,
                              0.01.sw,
                              0.01.sh,
                            ),
                            child: Center(
                              child: Text(
                                "Log in",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.0.sp,
                                  fontFamily: 'Jost',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 0.05.sw),
                      ArgonTimerButton(
                        width: 120,
                        height: 45,
                        minWidth: 100,
                        highlightColor: Colors.white,
                        highlightElevation: 5,
                        //splashColor: Colors.blue,
                        disabledColor: Colors.grey,
                        roundLoadingShape: true,
                        onTap: (startTimer, btnState) {
                          if (btnState == ButtonState.Idle) {
                            startTimer(20);
                          }
                        },
                        child: const Text(
                          "Renvoyer",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w700),
                        ),
                        loader: (timeLeft) {
                          return Text(
                            "Wait : $timeLeft",
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w700),
                          );
                        },
                        color: Colors.white,
                        elevation: 5,
                        borderRadius: 24.0,
                        //borderSide: BorderSide(color: Colors.black, width: 1.5),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          onClick
              ? Positioned.fill(
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.black.withOpacity(0.45),
                    child: const Center(
                      child: SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.grey,
                          color: Colors.purple,
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
}
