import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 8.0.sp),
              child: Text(
                "A propos",
                style: TextStyle(
                    fontSize: 25.sp,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Jost'),
              ),
            ),
            SizedBox(
              height: 15.sp,
            ),
            Expanded(
              child: ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0.sp),
                    child: Container(
                      padding: EdgeInsets.all(10.0.sp),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.withOpacity(0.1)),
                      child: Row(
                        children: [
                          TextButton.icon(
                            onPressed: () {},
                            icon: Icon(
                              Icons.star_outline_sharp,
                              color: Colors.black,
                              size: 22.sp,
                            ),
                            label: Text(
                              "Evaluer l'application",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0.sp),
                    child: Container(
                      padding: EdgeInsets.all(10.0.sp),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.withOpacity(0.1)),
                      child: Row(
                        children: [
                          TextButton.icon(
                            onPressed: () {},
                            icon: Icon(
                              Icons.thumb_up_outlined,
                              color: Colors.black,
                              size: 22.sp,
                            ),
                            label: Text(
                              "Aimez-nous sur facebook",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0.sp),
                    child: Container(
                      padding: EdgeInsets.all(10.0.sp),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.withOpacity(0.1)),
                      child: Row(
                        children: [
                          TextButton.icon(
                            onPressed: () {},
                            icon: Icon(
                              Icons.account_balance_outlined,
                              color: Colors.black,
                              size: 22.sp,
                            ),
                            label: Text(
                              "Mentions légales",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  [
                    Text(
                      "Fait avec le",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontFamily: 'Jost', fontSize: 13.sp),
                    ),
                    Icon(
                      Icons.favorite,
                      color: Colors.redAccent,
                      size: 13.sp,
                    ),
                    Text("à Proxiweb",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontFamily: 'Jost', fontSize: 13.sp)),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
