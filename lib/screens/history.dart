import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/models/request_taxi.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  List<RequestTaxi> reservations = [];
  bool loading = false;

  @override
  void initState() {
    setState(() {
      loading = true;
    });
    // TODO: implement initState
    super.initState();
    UserMethods.obtainReservations().then((value) {
      setState(() {
        loading = false;
      });
      setState(() {
        reservations = value!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
      ),
      body: loading == false
          ? Padding(
              padding: EdgeInsets.all(8.0.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0.sp),
                    child: Text(
                      "Historique des trajets",
                      style: TextStyle(
                          fontSize: 21.sp,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Jost'),
                    ),
                  ),
                  SizedBox(
                    height: 15.sp,
                  ),
                  ListView.builder(
                    itemCount: reservations.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      return Padding(
                        padding: EdgeInsets.all(5.0.sp),
                        child: Container(
                          padding: EdgeInsets.all(7.0.sp),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey.withOpacity(0.1)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(4.sp),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.grey.withOpacity(0.3)),
                                      child: const Icon(Icons.directions_car),
                                    ),
                                    SizedBox(
                                      width: 7.sp,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              "${reservations[i].location}- ${reservations[i].destination}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Jost',
                                                  fontSize: 11.sp,
                                                  overflow:
                                                      TextOverflow.ellipsis),
                                              overflow: TextOverflow.ellipsis),
                                          Text("${reservations[i].date}",
                                              style: TextStyle(
                                                  fontFamily: 'Jost',
                                                  fontSize: 10.sp,
                                                  overflow:
                                                      TextOverflow.ellipsis),
                                              overflow: TextOverflow.ellipsis)
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                width: 40,
                              ),
                              Text(
                                "${reservations[i].pricing} TND",
                                style: TextStyle(
                                    fontSize: 11.sp,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Jost'),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  )
                ],
              ),
            )
          : const Center(
              child: CircularProgressIndicator(
                color: Colors.orange,
              ),
            ),
    );
  }
}
