import 'package:flutter/material.dart';
import 'package:taxi_finder/assistants/user_methods.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _familyNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _cinController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  bool loading = false;
  String? photo;
  @override
  void initState() {
    setState(() {
      loading = true;
    });
    UserMethods.obtainProfileData().then((user) {
      setState(() {
        loading = false;
      });
      _nameController.text = user!.name!;
      _familyNameController.text = user.firstName!;
      _phoneController.text = user.phone!;
      _emailController.text = user.email!;
      _cinController.text = user.cin!;
      _addressController.text = user.address!;
      photo = user.photo;
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffB9B0A2),
      appBar: AppBar(
        backgroundColor:  Color(0xffB9B0A2),
        elevation: 0.0,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
      ),
      body: loading == false
          ? SingleChildScrollView(
              child: Column(
                children: [
                  const Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 16.0, top: 8.0),
                      child: Text(
                        "Mon profil",
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Jost'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(2), // Border width
                          decoration: const BoxDecoration(
                              color:  Color(0xffB9B0A2), shape: BoxShape.circle),
                          child: ClipOval(
                            child: SizedBox.fromSize(
                              size: const Size.fromRadius(45), // Image radius
                              child: Image.network("https://smartcity.proxiweb.tn/storage/app/" + (photo ?? ''), fit: BoxFit.fill,),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Prénom",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _nameController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Nom",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _familyNameController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Téléphone",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _phoneController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Email",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _emailController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "CIN",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _cinController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 5.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Addresse",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 40, right: 40, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: TextField(
                              controller: _addressController,
                              style: const TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: "Jost",
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintStyle: const TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                                fillColor: Colors.white,
                                filled: true,
                                isDense: true,
                                contentPadding: const EdgeInsets.only(
                                    left: 11.0, top: 8.0, bottom: 8.0),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(40.0, 30.0, 40.0, 10.0),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.orange,
                                textStyle: const TextStyle(color: Colors.white),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(24.0))),
                            onPressed: () {},
                            child: const SizedBox(
                              height: 50.0,
                              child: Center(
                                child: Text(
                                  "Enregistrer",
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      fontFamily: 'Jost',
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
          : const Center(
              child: CircularProgressIndicator(
                color: Colors.orange,
              ),
            ),
    );
  }
}
