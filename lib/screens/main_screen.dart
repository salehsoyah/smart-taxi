import 'dart:async';
import 'dart:convert';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_finder/assistants/assistant_methods.dart';
import 'package:taxi_finder/assistants/geo_fire_assistant.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/main.dart';
import 'package:taxi_finder/models/address.dart';
import 'package:taxi_finder/models/direction_details.dart';
import 'package:taxi_finder/models/nearby_available_drivers.dart';
import 'package:taxi_finder/models/request_taxi.dart';
import 'package:taxi_finder/screens/about.dart';
import 'package:taxi_finder/screens/history.dart';
import 'package:taxi_finder/screens/profile.dart';
import 'package:taxi_finder/screens/ratingScreen.dart';
import 'package:taxi_finder/screens/search_screen.dart';
import 'package:taxi_finder/widgets/collect_fares_dialog.dart';
import 'package:taxi_finder/widgets/divider.dart';
import 'package:taxi_finder/widgets/no_driver_availableDialog.dart';
import 'package:taxi_finder/widgets/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:label_marker/label_marker.dart';
import 'package:fast_immutable_collections/fast_immutable_collections.dart';

class MainScreen extends StatefulWidget {
  static const String idScreen = "mainScreen";
  String type;

  MainScreen({Key? key, required this.type}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with TickerProviderStateMixin {
  final Completer<GoogleMapController> _googleMapController = Completer();
  late GoogleMapController newGoogleMapController;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  DirectionDetails? tripDirectionDetails;

  List<LatLng> pLineCoordinates = [];
  List<LatLng> pLineCoordinates1 = [];
  List<LatLng> pLineCoordinates2 = [];

  Set<Polyline> polylineSet = {};
  Set<Marker> markersSet = {};
  Set<Circle> circlesSet = {};
  late Position currentPosition;
  var geoLocator = Geolocator();
  double bottomPaddingOfMap = 0;

  double rideDetailsContainer = 0;
  double requestRideDetailsContainer = 0;
  double searchContainerHeight = 0.22.sh;
  double driverDetailsContainer = 0;
  double pinContainer = 0.0;
  bool drawerOpen = true;
  bool nearByAvailableDriverKeysLoaded = false;

  BitmapDescriptor? nearByIcon;

  double kmPricing = 0;
  double timePricing = 0;
  double coef = 0;
  String state = "normal";
  StreamSubscription? rideStreamSubscription;
  bool isRequestingPositionDetails = false;
  List<NearbyAvailableDrivers>? availableDrivers = [];
  String userName = '';
  int? userId;

  CameraPosition? cameraPosition;

  String pinTitle = "";
  bool choosePin = false;

  bool pickedSmart = true;
  bool pickedProtect = false;
  bool pickedConfort = false;

  double? driverDistance;
  int? driverTime;
  String userPhone = '';
  bool initiatedDrivers = false;
  int val = 2;
  int suitCases = 0;
  final TextEditingController _controller = TextEditingController();
  bool arrived = false;
  NearbyAvailableDrivers? driver;
  Address airportAddress = Address(
      placeName: 'Sfax Thyna International Airpot',
      placeId: '',
      latitude: 34.7236,
      longitude: 10.6900);

  Address marketAddress = Address(
      placeName: 'Souk El Hout',
      placeId: '',
      latitude: 34.7383891,
      longitude: 10.7592904);

  Address stadiumAddress = Address(
      placeName: 'Stade taieb mhiri',
      placeId: '',
      latitude: 34.7335902,
      longitude: 10.7460751);

  Address theaterAddress = Address(
      placeName: 'Théâtre Municipal De Sfax',
      placeId: '',
      latitude: 34.7338,
      longitude: 10.7631);

  displayRequestRideContainer() {
    setState(() {
      requestRideDetailsContainer = 0.27.sh;
      rideDetailsContainer = 0;
      bottomPaddingOfMap = 230.0;
      drawerOpen = true;
    });

    saveRideRequest(widget.type);
  }

  List<Address> pathList = [
    Address(
        placeFormattedAddress: '',
        placeName: 'Gremda-1',
        placeId: '',
        latitude: 34.785272,
        longitude: 10.721017,
        path: '1'),
    Address(
        placeFormattedAddress: '',
        placeName: 'Gremda-2',
        placeId: '',
        latitude: 34.793211,
        longitude: 10.715427,
        path: '1'),
    Address(
        placeFormattedAddress: '',
        placeName: 'Gremda-3',
        placeId: '',
        latitude: 34.809286,
        longitude: 10.697881,
        path: '1'),
    Address(
        placeFormattedAddress: '',
        placeName: 'Tunis-1',
        placeId: '',
        latitude: 34.804978,
        longitude: 10.761418,
        path: '2'),
    Address(
        placeFormattedAddress: '',
        placeName: 'Tunis-2',
        placeId: '',
        latitude: 34.813973,
        longitude: 10.761918,
        path: '2'),
    Address(
        placeFormattedAddress: '',
        placeName: 'Tunis-3',
        placeId: '',
        latitude: 34.836437,
        longitude: 10.764813,
        path: '2'),
  ];

  List<Address> pathPredictionsList = [];

  void displayDriverDetailsContainer() {
    setState(() {
      requestRideDetailsContainer = 0.0;
      rideDetailsContainer = 0;
      bottomPaddingOfMap = 250.0;
      driverDetailsContainer = 0.35.sh;
    });
  }

  resetApp() {
    setState(() {
      drawerOpen = true;
      choosePin = false;
      pinTitle = '';
      searchContainerHeight = 0.22.sh;
      requestRideDetailsContainer = 0;
      rideDetailsContainer = 0;
      bottomPaddingOfMap = 230.0;
      pinContainer = 0.0;
      polylineSet.clear();
      markersSet.clear();
      circlesSet.clear();
      pLineCoordinates.clear();
      pLineCoordinates1.clear();
      pLineCoordinates2.clear();
      suitCases = 0;
      _controller.text = '0';
      val = 2;
      arrived = false;
      statusRide = "";
      driverName = "";
      driverPhoneNumber = "";
      cardDetailsDriver = "";
      rideStatus = "Chauffeur en route";
      driverDetailsContainer = 0.0;
    });

    locatePosition();
  }

  void displayPinContainer() async {
    setState(() {
      choosePin = true;
      searchContainerHeight = 0;
      rideDetailsContainer = 0.0;
      pinContainer = 0.17.sh;
      bottomPaddingOfMap = 230.0;
      drawerOpen = false;
    });
  }

  void displayRideDetailsContainer() async {
    if (Provider.of<AppData>(context, listen: false).appType == 2) {
      AssistantMethods.obtainRoutePricing().then((value) {
        if (value != null) {
          setState(() {
            kmPricing = value.kmPrice!;
            timePricing = value.timePrice!;
            coef = value.coef!;
          });
        } else {
          displayToastMessage(
              "Erreur: Vérifiez votre connection internet", context);
        }
      });
      await getPlaceDirection(context);
    } else {
      await getPlaceDirectionPlace(context);
    }
    setState(() {
      searchContainerHeight = 0;
      rideDetailsContainer = 0.3.sh;
      bottomPaddingOfMap = 230.0;
      pinContainer = 0.0;
      drawerOpen = false;
    });
  }

  void locatePosition() async {
    LocationPermission permission;
    permission = await Geolocator.requestPermission();

    // if (permission != LocationPermission.denied ||
    //     permission != LocationPermission.deniedForever) {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      currentPosition = position;
    });

    LatLng latLngPosition = LatLng(position.latitude, position.longitude);

    CameraPosition cameraPosition =
        CameraPosition(target: latLngPosition, zoom: 14);

    newGoogleMapController
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

    String address =
        await AssistantMethods.searchCoordinateAddress(position, context);
    initGeoFireListener();

    // print("This is your address :: " + address);
  }

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(34.7398, 10.7600),
    zoom: 2.4746,
  );

  @override
  void dispose() {
    newGoogleMapController.dispose();
    streamController.close();
    super.dispose();
  }

  getApi() async {
    final url = Uri.parse('http://192.168.1.97:8000/api/user');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      streamController.sink.add(data);
    } else {
      throw Exception();
    }
  }

  StreamController streamController = StreamController();
  StreamSubscription? streamSub;
  final List<dynamic> _list = [];
  String? photo;

  String actionTaken(dynamic data) {
    return data;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller.text = "0";
    getUsername().then((value) {
      setState(() {
        userName = value!;
      });
    });
    getUserPhone().then((value) {
      setState(() {
        userPhone = value!;
      });
    });
    getUserId().then((value) {
      setState(() {
        userId = value!;
      });
    });
    UserMethods.obtainProfileData().then((user) {
      photo = user!.photo;
    });
  }

  saveRideRequest(String type) async {
    rideRequestRef =
        FirebaseDatabase.instance.reference().child("Ride Requests").push();
    var pickUp = Provider.of<AppData>(context, listen: false).pickUpLocation;
    var dropOff = Provider.of<AppData>(context, listen: false).dropOffLocation;
    var fares = Provider.of<AppData>(context, listen: false).fares;
    var providerD = Provider.of<AppData>(context, listen: false);

    Map pickUpLocMap = {
      "latitude": pickUp!.latitude.toString(),
      "longitude": pickUp.longitude.toString(),
    };

    Map dropOffLocMap = {
      "latitude": dropOff!.latitude.toString(),
      "longitude": dropOff.longitude.toString(),
    };

    Map rideInfoMap = {
      "driver_id": "waiting",
      "payment_method": "cash",
      "type": type,
      "pickUp": pickUpLocMap,
      "dropOff": dropOffLocMap,
      "created_at": DateTime.now().toString(),
      "ride_name": userName,
      "ride_phone": userPhone,
      "ride_id": userId,
      "pickup_address": pickUp.placeName,
      "dropoff_address": dropOff.placeName,
      "fares": providerD.appType == 1 ? providerD.placeFee : fares!
    };

    rideRequestRef.set(rideInfoMap);

    rideStreamSubscription = rideRequestRef.onValue.listen((event) async {
      print(event.snapshot.key.toString() + ' mpiufz');
      if (event.snapshot.value == null) {
        return;
      }
      Map eventMap = event.snapshot.value as Map;
      if (eventMap['status'] != null) {
        statusRide = eventMap["status"].toString();
      }
      if (eventMap['car_details'] != null) {
        setState(() {
          cardDetailsDriver = eventMap["car_details"].toString();
        });
      }
      if (eventMap['driver_name'] != null) {
        setState(() {
          driverName = eventMap["driver_name"].toString();
        });
      }
      if (eventMap['driver_phone'] != null) {
        setState(() {
          driverPhoneNumber = eventMap["driver_phone"].toString();
        });
      }
      if (eventMap['driver_location'] != null) {
        double driverLat =
            double.parse(eventMap["driver_location"]["latitude"]);
        double driverLng =
            double.parse(eventMap["driver_location"]["longitude"]);
        LatLng driverCurrentLocation = LatLng(driverLat, driverLng);
        if (statusRide == "accepted") {
          updateRideTimeToPickUpLoc(driverCurrentLocation);
        } else if (statusRide == "arrived") {
          setState(() {
            rideStatus = "Chauffeur arrivé.";
            arrived = true;
          });
        } else if (statusRide == "onride") {
          updateRideTimeToDropOffLoc(driverCurrentLocation);
        }
      }
      if (statusRide == "accepted") {
        displayDriverDetailsContainer();
        Geofire.stopListener();
        deleteGeoFireMarkers();
      }
      if (statusRide == "ended") {
        if (eventMap['fares'] != null) {
          double fare = double.parse(eventMap['fares'].toString());
          var res = await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => CollectFaresDialog(
                paymentMethod: "En espèces", fareAmount: fare),
          );
          // saveRideRequestSql(context, eventMap['driver_id'], eventMap['fares'].toString(), statusRide);
          String driverId = "";
          if (res == "close") {
            if (eventMap["driver_id"] != null) {
              driverId = eventMap["driver_id"].toString();
            }
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RatingDialog(
                      driverId: driverId,
                    )));
            // rideRequestRef.remove();
            rideRequestRef.onDisconnect();
            rideStreamSubscription!.cancel();
            rideStreamSubscription = null;
            resetApp();
          }
        }
      }
    });
  }

  void saveRideRequestSql(
      context, String driverId, String fares, String status) async {
    String url = "https://smartcity.proxiweb.tn/api/reservationkm/save";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    int? clientId = sharedPreferences.getInt('userId');
    var pickUp = Provider.of<AppData>(context, listen: false).pickUpLocation;
    var dropOff = Provider.of<AppData>(context, listen: false).dropOffLocation;

    var queryParameters = {
      'id_taxi': driverId,
      'id_client': clientId,
      'depart': pickUp!.latitude.toString() + ',' + pickUp.longitude.toString(),
      'arrive':
          dropOff!.latitude.toString() + ',' + dropOff.longitude.toString(),
      'nom_depart': pickUp.placeName,
      'nom_arrivee': dropOff.placeName,
      'tarif': fares,
      'status': status,
      'nom_client': userName,
      'tel_client': userPhone,
    };

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) =>
          ProgressDialog(message: "Veuillez patienter..."),
    );
    Response response = await http.post(Uri.parse(url),
        // Send authorization headers to the backend.
        headers: {
          "content-type": "application/json",
        },
        body: json.encode(queryParameters));
    Navigator.pop(context);
    var body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      if (body["data"]["code"] == 200) {
        print("savedpou");
      } else {}
    } else {}
  }

  void deleteGeoFireMarkers() {
    markersSet
        .removeWhere((element) => element.markerId.value.contains("driver"));
  }

  void updateRideTimeToPickUpLoc(LatLng driverCurrentLocation) async {
    if (isRequestingPositionDetails == false) {
      isRequestingPositionDetails = true;
      var positionUserLatLng =
          LatLng(currentPosition.latitude, currentPosition.longitude);
      var details = await AssistantMethods.obtainDirectionDetails(
          driverCurrentLocation, positionUserLatLng);
      if (details == null) {
        return;
      }
      if (!mounted) return;

      setState(() {
        rideStatus = "Chauffeur en route - " + details.durationText!;
      });
      isRequestingPositionDetails = false;
    }
  }

  void updateRideTimeToDropOffLoc(LatLng driverCurrentLocation) async {
    if (isRequestingPositionDetails == false) {
      isRequestingPositionDetails = true;
      var dropOff =
          Provider.of<AppData>(context, listen: false).dropOffLocation;
      var dropOffUserLatLng = LatLng(dropOff!.latitude, dropOff.longitude);
      var details = await AssistantMethods.obtainDirectionDetails(
          driverCurrentLocation, dropOffUserLatLng);
      if (details == null) {
        return;
      }
      setState(() {
        rideStatus = "En route vers la destination - " + details.durationText!;
      });
      isRequestingPositionDetails = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    var providerData = Provider.of<AppData>(context);
    createIconMarker();
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        toolbarHeight: 0.08.sh,
        backgroundColor: Colors.orange,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      drawer: Container(
        color: Colors.white,
        width: 0.6.sw,
        child: Drawer(
          child: ListView(
            children: [
              SizedBox(
                height: 0.31.sw,
                child: DrawerHeader(
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(2), // Border width
                        decoration: const BoxDecoration(
                            color: Colors.orange, shape: BoxShape.circle),
                        child: ClipOval(
                          child: SizedBox.fromSize(
                            size: const Size.fromRadius(30), // Image radius
                            child: Image.network(
                              "https://smartcity.proxiweb.tn/storage/app/" +
                                  (photo ?? ''),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 0.02.sw,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            userName,
                            style: TextStyle(
                                fontSize: 14.0.sp,
                                fontFamily: "Jost",
                                fontWeight: FontWeight.bold),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const Profile()));
                            },
                            child: Text(
                              "Visiter le profil",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold,
                                fontSize: 10.0.sp,
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                decoration: const BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15)),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.fromLTRB(0.02.sw, 0.02.sh, 0.02.sw, 0.02.sh),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextButton.icon(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const History()));
                        },
                        icon: Icon(
                          Icons.history,
                          color: Colors.white,
                          size: 18.sp,
                        ),
                        label: Text(
                          "Historique des trajets",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Jost',
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0.sp,
                          ),
                        ),
                      ),
                      TextButton.icon(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Profile()));
                        },
                        icon: Icon(
                          Icons.person,
                          color: Colors.white,
                          size: 18.sp,
                        ),
                        label: Text(
                          "Visiter le profil",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Jost',
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0.sp,
                          ),
                        ),
                      ),
                      TextButton.icon(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const About()));
                        },
                        icon: Icon(
                          Icons.info,
                          color: Colors.white,
                          size: 18.sp,
                        ),
                        label: Text(
                          "A propos",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Jost',
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0.sp,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          GoogleMap(
            padding: EdgeInsets.only(bottom: bottomPaddingOfMap),
            mapType: MapType.normal,
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            zoomGesturesEnabled: true,
            zoomControlsEnabled: true,
            initialCameraPosition: _kGooglePlex,
            polylines: polylineSet,
            markers: markersSet,
            circles: circlesSet,
            onMapCreated: (GoogleMapController controller) {
              _googleMapController.complete(controller);
              newGoogleMapController = controller;
              setState(() {
                bottomPaddingOfMap = 300.0;
              });

              locatePosition();
            },
            onCameraMove: (CameraPosition cameraNewPosition) async {
              setState(() {
                cameraPosition = cameraNewPosition;
              });
            },
            onCameraIdle: () async {
              getCameraPositionData();
            },
          ),
          choosePin == true
              ? Center(
                  //picker image on google map
                  child: Image.asset(
                    "assets/images/pin.png",
                    width: 0.06.sw,
                  ),
                )
              : Container(),
          Positioned(
            top: 0.05.sh,
            left: 0.05.sw,
            child: GestureDetector(
              onTap: () {
                if (drawerOpen) {
                  scaffoldKey.currentState?.openDrawer();
                } else {
                  resetApp();
                }
              },
              child: Container(
                padding:
                    EdgeInsets.fromLTRB(0.03.sw, 0.025.sh, 0.03.sw, 0.025.sh),
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.orange,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2.0,
                        spreadRadius: 0.3,
                        offset: Offset(0.3, 0.3),
                      )
                    ]),
                child: Icon(
                  ((drawerOpen) ? Icons.menu : Icons.close),
                  color: Colors.white,
                  size: 22.sp,
                ),
              ),
            ),
          ),
          driverDetailsContainer > 0 && !arrived
              ? Positioned(
                  top: 0.05.sh,
                  right: 0.05.sw,
                  child: GestureDetector(
                    onTap: () {
                      resetApp();
                      rideRequestRef.child('status').once().then((status) {
                        if (status.snapshot.value != null) {
                          rideRequestRef.child('status').set('cancelled');
                        }
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(
                          0.03.sw, 0.025.sh, 0.03.sw, 0.025.sh),
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.orange,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.3,
                              offset: Offset(0.3, 0.3),
                            )
                          ]),
                      child: Icon(
                        (Icons.close),
                        color: Colors.white,
                        size: 22.sp,
                      ),
                    ),
                  ),
                )
              : Container(),
          Positioned(
            left: 0.0,
            right: 0.0,
            bottom: 0.0,
            child: AnimatedSize(
              vsync: this,
              curve: Curves.bounceIn,
              duration: const Duration(milliseconds: 160),
              child: Container(
                height: searchContainerHeight,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(18.0),
                        topRight: Radius.circular(18.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 16.0,
                        spreadRadius: 0.5,
                        offset: Offset(0.7, 0.7),
                      )
                    ]),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 0.05.sw, vertical: 0.015.sh),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 0.01.sh,
                      ),
                      GestureDetector(
                        onTap: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchScreen(
                                        type: widget.type,
                                      ))).then((value) async {
                            if (value == true) {
                              if (GeoFireAssistant
                                  .nearByAvailableDriversList.isEmpty) {
                                cancelRideRequest();
                                resetApp();
                                noDriverFound();
                                return;
                              }
                              var driver = GeoFireAssistant
                                  .nearByAvailableDriversList.first;

                              LatLng userPosition = LatLng(
                                  currentPosition.latitude,
                                  currentPosition.longitude);
                              LatLng driverPosition =
                                  LatLng(driver.latitude!, driver.longitude!);
                              var details =
                                  await AssistantMethods.obtainDirectionDetails(
                                      driverPosition, userPosition);

                              var distance = details?.distanceValue!;
                              var time = details?.durationValue!;
                              setState(() {
                                driverDistance = distance! / 1000;
                                driverTime = time;
                              });

                              if (providerData.appType == 1) {
                                for (var nearbyDriver in GeoFireAssistant
                                    .nearByAvailableDriversList) {
                                  var details = await AssistantMethods
                                      .obtainDirectionDetails(
                                          LatLng(nearbyDriver.latitude!,
                                              nearbyDriver.longitude!),
                                          LatLng(
                                              providerData
                                                  .pickUpStation!.latitude!,
                                              providerData
                                                  .pickUpStation!.longitude!));
                                  setState(() {
                                    nearbyDriver.time =
                                        (details!.durationValue! / 60)
                                            .truncate();
                                  });

                                  driverRef
                                      .child(nearbyDriver.key!)
                                      .once()
                                      .then((value) {
                                    if (value.snapshot.value != null) {
                                      Map eventMap =
                                          value.snapshot.value as Map;
                                      setState(() {
                                        nearbyDriver.route = eventMap['route'];
                                        nearbyDriver.direction =
                                            eventMap['direction'];
                                        nearbyDriver.availablePlaces =
                                            int.parse(
                                                eventMap['availablePlaces']);
                                      });
                                    }
                                  });
                                }
                                AssistantMethods()
                                    .obtainPricePlace(

                                        providerData.pickUpStation!.idRoute!,
                                        providerData.pickUpStation!.order,
                                        providerData.dropOffStation!.order)
                                    .then((value) {
                                  Provider.of<AppData>(context, listen: false)
                                      .updatePlaceFee(value!);
                                });
                              }

                              displayRideDetailsContainer();
                            } else if (value == "pin") {
                              displayPinContainer();
                            }
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: const [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.3,
                                  offset: Offset(0.3, 0.3),
                                )
                              ]),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(
                                0.03.sw, 0.015.sh, 0.03.sw, 0.015.sh),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.search,
                                  color: Colors.orange,
                                  size: 20.sp,
                                ),
                                SizedBox(
                                  width: 0.02.sw,
                                ),
                                Text(
                                  "Où allez-vous ?",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Jost',
                                      fontSize: 16.sp),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 0.02.sh,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.location_pin,
                              color: Colors.blue,
                              size: 20.sp,
                            ),
                            SizedBox(
                              width: 0.02.sw,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    Provider.of<AppData>(context)
                                                .pickUpLocation !=
                                            null
                                        ? Provider.of<AppData>(context)
                                            .pickUpLocation!
                                            .placeName
                                        : "Ajoutez le lieu de prise en charge",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Jost',
                                        fontSize: 15.sp,
                                        overflow: TextOverflow.ellipsis),
                                    textAlign: TextAlign.start,
                                  ),
                                  SizedBox(
                                    height: 0.01.sh,
                                  ),
                                  Text(
                                    "Lieu de prise en charge",
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Jost',
                                      fontSize: 14.sp,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 0.01.sh,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: AnimatedSize(
              vsync: this,
              curve: Curves.bounceIn,
              duration: const Duration(milliseconds: 160),
              child: Container(
                height: (rideDetailsContainer > 0 && widget.type == '1')
                    ? rideDetailsContainer + 40
                    : rideDetailsContainer,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16.0),
                        topRight: Radius.circular(16.0)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black,
                          blurRadius: 16.0,
                          spreadRadius: 0.5,
                          offset: Offset(0.7, 0.7))
                    ]),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.02.sh),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        providerData.appType == 1 &&
                                providerData.closestPointDropOff != null
                            ? Container(
                                decoration: BoxDecoration(
                                    color: Colors.green.withOpacity(0.2)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: Column(
                                          children: [
                                            Text(
                                              providerData
                                                  .pickUpLocation!.placeName,
                                              style: TextStyle(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Jost'),
                                            ),
                                            Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.blue,
                                            )
                                          ],
                                        ),
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            (Geolocator.distanceBetween(
                                                          providerData
                                                              .pickUpLocation!
                                                              .latitude,
                                                          providerData
                                                              .pickUpLocation!
                                                              .longitude,
                                                          providerData
                                                              .closestPointPickUp!
                                                              .latitude,
                                                          providerData
                                                              .closestPointPickUp!
                                                              .longitude,
                                                        ) /
                                                        1000)
                                                    .toStringAsFixed(2) +
                                                'Km',
                                            style: TextStyle(fontSize: 11),
                                          ),
                                          Icon(
                                            Icons.arrow_forward,
                                            size: 11,
                                          )
                                        ],
                                      ),
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: Column(
                                          children: [
                                            Text(
                                              providerData.closestPointPickUp!
                                                  .placeName,
                                              style: TextStyle(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Jost'),
                                            ),
                                            Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.green,
                                            )
                                          ],
                                        ),
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            (Geolocator.distanceBetween(
                                                          providerData
                                                              .closestPointPickUp!
                                                              .latitude,
                                                          providerData
                                                              .closestPointPickUp!
                                                              .longitude,
                                                          providerData
                                                              .closestPointDropOff!
                                                              .latitude,
                                                          providerData
                                                              .closestPointDropOff!
                                                              .longitude,
                                                        ) /
                                                        1000)
                                                    .toStringAsFixed(2) +
                                                'Km',
                                            style: TextStyle(fontSize: 11),
                                          ),
                                          Icon(
                                            Icons.arrow_forward,
                                            size: 11,
                                          )
                                        ],
                                      ),
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: Column(
                                          children: [
                                            Text(
                                              providerData.closestPointDropOff!
                                                  .placeName,
                                              style: TextStyle(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Jost'),
                                            ),
                                            Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.orange,
                                            )
                                          ],
                                        ),
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            (Geolocator.distanceBetween(
                                                          providerData
                                                              .closestPointDropOff!
                                                              .latitude,
                                                          providerData
                                                              .closestPointDropOff!
                                                              .longitude,
                                                          providerData
                                                              .dropOffLocation!
                                                              .latitude,
                                                          providerData
                                                              .dropOffLocation!
                                                              .longitude,
                                                        ) /
                                                        1000)
                                                    .toStringAsFixed(2) +
                                                'Km',
                                            style: TextStyle(fontSize: 11),
                                          ),
                                          Icon(
                                            Icons.arrow_forward,
                                            size: 11,
                                          )
                                        ],
                                      ),
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: Column(
                                          children: [
                                            Text(
                                              providerData
                                                  .dropOffLocation!.placeName,
                                              style: TextStyle(
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Jost'),
                                            ),
                                            const Icon(
                                              Icons.location_on_rounded,
                                              color: Colors.red,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : Container(),
                        Provider.of<AppData>(context).appType == 2
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    pickedSmart = true;
                                    pickedProtect = false;
                                    pickedConfort = false;
                                  });
                                  displayToastMessage(
                                      "Vous avez choisi Smart Taxi", context);
                                },
                                child: Container(
                                  width: double.infinity,
                                  color: pickedSmart
                                      ? Colors.orange[50]
                                      : Colors.grey[50],
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 0.04.sw),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'assets/images/taxi.png',
                                          width: 0.2.sw,
                                        ),
                                        SizedBox(
                                          width: 0.04.sw,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Voiture",
                                              style: TextStyle(
                                                  fontSize: 16.0.sp,
                                                  fontFamily: "Jost",
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              tripDirectionDetails != null
                                                  ? tripDirectionDetails!
                                                      .distanceText!
                                                  : '',
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  fontFamily: "Jost",
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey),
                                            ),
                                            Text(
                                              'Attente de ${Provider.of<AppData>(context).driverTime!.truncate()} min',
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  fontFamily: "Jost",
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                        Expanded(child: Container()),
                                        Text(
                                          ((tripDirectionDetails != null
                                              ? '${(AssistantMethods.calculateFares(tripDirectionDetails!, driverDistance!, driverTime!, timePricing, kmPricing, coef, context) + suitCases).toStringAsFixed(2)} TND'
                                              : '')),
                                          style: TextStyle(
                                              fontSize: 16.0.sp,
                                              fontFamily: "Jost",
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : providerData.pickUpStation != null
                                ? ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: GeoFireAssistant
                                                    .getDriversByOptions(
                                                        context)
                                                .length >
                                            3
                                        ? 3
                                        : GeoFireAssistant.getDriversByOptions(
                                                context)
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              driver = GeoFireAssistant.getDriversByOptions(context)[index];
                                              print(driver);
                                              pickedSmart = true;
                                              pickedProtect = false;
                                              pickedConfort = false;
                                            });
                                            displayToastMessage(
                                                "Vous avez choisi Smart Taxi",
                                                context);
                                          },
                                          child: Container(
                                            width: double.infinity,
                                            color: pickedSmart
                                                ? Colors.orange[50]
                                                : Colors.grey[50],
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 0.04.sw),
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/taxi.png',
                                                    width: 0.2.sw,
                                                  ),
                                                  SizedBox(
                                                    width: 0.04.sw,
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        "Voiture",
                                                        style: TextStyle(
                                                            fontSize: 16.0.sp,
                                                            fontFamily: "Jost",
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        '${GeoFireAssistant.getDriversByOptions(context)[index].availablePlaces.toString()} places disponibles ',
                                                        style: TextStyle(
                                                            fontSize: 14.sp,
                                                            fontFamily: "Jost",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.5)),
                                                      ),
                                                      Text(
                                                        'Arrive dans ${GeoFireAssistant.getDriversByOptions(context)[index].time.toString()} min',
                                                        style: TextStyle(
                                                            fontSize: 14.sp,
                                                            fontFamily: "Jost",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors.grey),
                                                      ),
                                                    ],
                                                  ),
                                                  Expanded(child: Container()),
                                                  Text(
                                                    '${providerData.placeFee} TND',
                                                    style: TextStyle(
                                                        fontSize: 16.0.sp,
                                                        fontFamily: "Jost",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    })
                                : Container(),
                        SizedBox(
                          height: 0.015.sh,
                        ),
                        if (widget.type == '2') ...[
                          const Divider(
                            thickness: 2.0,
                            height: 2.0,
                          ),
                          SizedBox(
                            height: 0.015.sh,
                          ),
                        ],
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.04.sw),
                          child: Row(
                            children: [
                              Icon(
                                FontAwesomeIcons.moneyCheckAlt,
                                size: 18.0.sp,
                                color: Colors.green,
                              ),
                              SizedBox(
                                width: 0.04.sw,
                              ),
                              Text(
                                "Espèces",
                                style: TextStyle(
                                    fontSize: 14.0.sp,
                                    fontFamily: "Jost",
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 0.015.sw,
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.black54,
                                size: 16.0.sp,
                              ),
                            ],
                          ),
                        ),
                        // Padding(
                        //     padding: EdgeInsets.symmetric(horizontal: 0.04.sw),
                        //     child: Column(
                        //         crossAxisAlignment: CrossAxisAlignment.start,
                        //         children: [
                        //           Row(
                        //             crossAxisAlignment:
                        //                 CrossAxisAlignment.center,
                        //             children: [
                        //               Icon(
                        //                 FontAwesomeIcons.suitcase,
                        //                 size: 18.0.sp,
                        //                 color: Colors.brown,
                        //               ),
                        //               SizedBox(
                        //                 width: 0.04.sw,
                        //               ),
                        //               Text(
                        //                 "Bagage",
                        //                 style: TextStyle(
                        //                     fontSize: 14.0.sp,
                        //                     fontFamily: "Jost",
                        //                     fontWeight: FontWeight.bold),
                        //               ),
                        //               SizedBox(
                        //                 width: 0.04.sw,
                        //               ),
                        //               Text(
                        //                 "Oui",
                        //                 style: TextStyle(
                        //                     fontSize: 12.0.sp,
                        //                     fontFamily: "Jost",
                        //                     fontWeight: FontWeight.bold),
                        //               ),
                        //               Radio(
                        //                 value: 1,
                        //                 groupValue: val,
                        //                 onChanged: (value) {
                        //                   if (value != null) {
                        //                     setState(() {
                        //                       val = 1;
                        //                     });
                        //                   }
                        //                 },
                        //                 activeColor: Colors.orange,
                        //               ),
                        //               Text(
                        //                 "Non",
                        //                 style: TextStyle(
                        //                     fontSize: 12.0.sp,
                        //                     fontFamily: "Jost",
                        //                     fontWeight: FontWeight.bold),
                        //               ),
                        //               Radio(
                        //                 value: 2,
                        //                 groupValue: val,
                        //                 onChanged: (value) {
                        //                   if (value != null) {
                        //                     setState(() {
                        //                       val = 2;
                        //                     });
                        //                   }
                        //                 },
                        //                 activeColor: Colors.orange,
                        //               ),
                        //               val == 1
                        //                   ? Expanded(
                        //                       child: Container(
                        //                         foregroundDecoration:
                        //                             BoxDecoration(
                        //                           borderRadius:
                        //                               BorderRadius.circular(
                        //                                   5.0),
                        //                           border: Border.all(
                        //                             color: Colors.orange,
                        //                             width: 2.0,
                        //                           ),
                        //                         ),
                        //                         child: Row(
                        //                           children: <Widget>[
                        //                             Expanded(
                        //                               flex: 1,
                        //                               child: TextFormField(
                        //                                 enabled: false,
                        //                                 textAlign:
                        //                                     TextAlign.center,
                        //                                 decoration:
                        //                                     InputDecoration(
                        //                                   contentPadding:
                        //                                       const EdgeInsets
                        //                                           .all(8.0),
                        //                                   border:
                        //                                       OutlineInputBorder(
                        //                                     borderRadius:
                        //                                         BorderRadius
                        //                                             .circular(
                        //                                                 5.0),
                        //                                   ),
                        //                                 ),
                        //                                 controller: _controller,
                        //                                 keyboardType:
                        //                                     const TextInputType
                        //                                         .numberWithOptions(
                        //                                   decimal: false,
                        //                                   signed: true,
                        //                                 ),
                        //                               ),
                        //                             ),
                        //                             SizedBox(
                        //                               height: 38.0,
                        //                               child: Column(
                        //                                 crossAxisAlignment:
                        //                                     CrossAxisAlignment
                        //                                         .center,
                        //                                 mainAxisAlignment:
                        //                                     MainAxisAlignment
                        //                                         .center,
                        //                                 children: <Widget>[
                        //                                   Container(
                        //                                     decoration:
                        //                                         const BoxDecoration(
                        //                                       border: Border(
                        //                                         bottom:
                        //                                             BorderSide(
                        //                                           width: 0.5,
                        //                                         ),
                        //                                       ),
                        //                                     ),
                        //                                     child: InkWell(
                        //                                       child: const Icon(
                        //                                         Icons
                        //                                             .arrow_drop_up,
                        //                                         size: 18.0,
                        //                                       ),
                        //                                       onTap: () {
                        //                                         int currentValue =
                        //                                             int.parse(
                        //                                                 _controller
                        //                                                     .text);
                        //                                         setState(() {
                        //                                           currentValue++;
                        //                                           _controller
                        //                                                   .text =
                        //                                               (currentValue)
                        //                                                   .toString();
                        //                                           suitCases =
                        //                                               currentValue *
                        //                                                   1;
                        //                                         });
                        //                                       },
                        //                                     ),
                        //                                   ),
                        //                                   InkWell(
                        //                                     child: const Icon(
                        //                                       Icons
                        //                                           .arrow_drop_down,
                        //                                       size: 18.0,
                        //                                     ),
                        //                                     onTap: () {
                        //                                       int currentValue =
                        //                                           int.parse(
                        //                                               _controller
                        //                                                   .text);
                        //                                       setState(() {
                        //                                         print(
                        //                                             "Setting state");
                        //                                         currentValue--;
                        //                                         _controller
                        //                                             .text = (currentValue >
                        //                                                     0
                        //                                                 ? currentValue
                        //                                                 : 0)
                        //                                             .toString();
                        //                                         if (currentValue >=
                        //                                             0) {
                        //                                           suitCases =
                        //                                               currentValue *
                        //                                                   1;
                        //                                         }
                        //                                       });
                        //                                     },
                        //                                   ),
                        //                                 ],
                        //                               ),
                        //                             ),
                        //                           ],
                        //                         ),
                        //                       ),
                        //                     )
                        //                   : Container()
                        //             ],
                        //           ),
                        //         ])),
                        SizedBox(
                          height: 0.02.sh,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0.05.sw),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.orange),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(24.0),
                                ),
                              ),
                            ),
                            onPressed: () async {
                              setState(() {
                                state = "requesting";
                              });
                              displayRequestRideContainer();
                              if (providerData.appType == 2) {
                                setState(() {
                                  availableDrivers = GeoFireAssistant
                                      .nearByAvailableDriversList;
                                });
                                searchNearestDriver();
                              } else{
                                notifyDriver(driver!);
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                  0.02.sw, 0.012.sh, 0.02.sw, 0.012.sh),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Confirmer",
                                    style: TextStyle(
                                        fontSize: 22.0.sp,
                                        fontFamily: "Jost",
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Icon(
                                    FontAwesomeIcons.taxi,
                                    color: Colors.white,
                                    size: 26.0.sp,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 0.02.sh,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              height: requestRideDetailsContainer,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 0.5,
                        blurRadius: 16.0,
                        color: Colors.black54,
                        offset: Offset(0.7, 0.7))
                  ]),
              child: Padding(
                padding: EdgeInsets.all(13.0.sp),
                child: Column(
                  children: [
                    SizedBox(height: 0.009.sh),
                    Flexible(
                        child: AnimatedTextKit(
                      repeatForever: true,
                      animatedTexts: [
                        ColorizeAnimatedText(
                          "S'il vous plaît, attendez...",
                          textStyle: TextStyle(
                              fontSize: 25.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          colors: [
                            Colors.orange,
                            Colors.orange,
                            Colors.orange,
                            Colors.orange,
                          ],
                        ),
                        ColorizeAnimatedText(
                          "Recherche d'un taxi...",
                          textStyle: TextStyle(
                              fontSize: 25.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          colors: [
                            Colors.orange,
                            Colors.orange,
                            Colors.orange,
                            Colors.orange,
                          ],
                        ),
                      ],
                      isRepeatingAnimation: true,
                      onTap: () {
                        print("Tap Event");
                      },
                    )),
                    SizedBox(height: 0.015.sh),
                    GestureDetector(
                      onTap: () {
                        cancelRideRequest();
                        resetApp();
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(
                          0.02.sw,
                          0.02.sw,
                          0.02.sw,
                          0.02.sw,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(26.0),
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1.0,
                                spreadRadius: 0.15,
                                offset: Offset(0.1, 0.1),
                              )
                            ]),
                        child: Icon(
                          Icons.close,
                          size: 32.0.sp,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(height: 0.01.sh),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        'Annuler le trajet',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold,
                          color: Colors.orange,
                          shadows: const <Shadow>[
                            Shadow(
                              offset: Offset(0.1, 0.1),
                              blurRadius: 1.0,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 0.5,
                        blurRadius: 16.0,
                        color: Colors.black54,
                        offset: Offset(0.7, 0.7))
                  ]),
              height: driverDetailsContainer,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 0.05.sw, vertical: 0.025.sh),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 0.009.sh),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text(
                            rideStatus,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18.0.sp,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    const Divider(
                      thickness: 2,
                    ),
                    SizedBox(height: 0.009.sh),
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 18.sp,
                          child: ClipOval(
                              child: Icon(
                            Icons.person,
                            size: 20.sp,
                          )),
                        ),
                        SizedBox(width: 0.03.sw),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              cardDetailsDriver,
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontFamily: 'Jost',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.sp),
                            ),
                            Text(
                              driverName,
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontFamily: 'Jost',
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 0.01.sh),
                    const Divider(
                      thickness: 2,
                    ),
                    SizedBox(height: 0.01.sh),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //call
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 0.02.sw),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {
                                launch('tel://$driverPhoneNumber');
                              },
                              color: Colors.green,
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                  0.02.sw,
                                  0.01.sh,
                                  0.02.sw,
                                  0.01.sh,
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      "Appeler",
                                      style: TextStyle(
                                          fontSize: 18.0.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontFamily: 'Jost'),
                                    ),
                                    Icon(
                                      Icons.call,
                                      color: Colors.white,
                                      size: 22.sp,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 0.5,
                        blurRadius: 16.0,
                        color: Colors.black54,
                        offset: Offset(0.7, 0.7))
                  ]),
              height: pinContainer,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 0.09.sw, vertical: 0.012.sh),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            pinTitle.isEmpty
                                ? Provider.of<AppData>(context)
                                            .pickUpLocation !=
                                        null
                                    ? Provider.of<AppData>(context)
                                        .pickUpLocation!
                                        .placeName
                                    : ''
                                : pinTitle,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18.0.sp,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 0.01.sh,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.orange,
                          textStyle: const TextStyle(color: Colors.white),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24.0))),
                      onPressed: () async {
                        chooseDestinationWithPinConfirmation();
                      },
                      child: SizedBox(
                        height: 0.065.sh,
                        child: Center(
                          child: Text(
                            "Confirmer la destination",
                            style: TextStyle(
                                fontSize: 19.0.sp,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //destinations
          drawerOpen
              ? Positioned(
                  top: 5,
                  left: 0,
                  right: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          chooseDestinationVIP(airportAddress);
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              0.010.sw, 0.010.sh, 0.010.sw, 0.010.sh),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.greenAccent,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.3,
                                  offset: Offset(0.3, 0.3),
                                )
                              ]),
                          child: Icon(
                            (Icons.airplanemode_active),
                            color: Colors.white,
                            size: 22.sp,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          chooseDestinationVIP(marketAddress);
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              0.010.sw, 0.010.sh, 0.010.sw, 0.010.sh),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.lightBlueAccent,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.3,
                                  offset: Offset(0.3, 0.3),
                                )
                              ]),
                          child: Icon(
                            (Icons.shopping_cart),
                            color: Colors.white,
                            size: 22.sp,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          chooseDestinationVIP(stadiumAddress);
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              0.010.sw, 0.010.sh, 0.010.sw, 0.010.sh),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.3,
                                  offset: Offset(0.3, 0.3),
                                )
                              ]),
                          child: Icon(
                            (Icons.sports_volleyball_outlined),
                            color: Colors.white,
                            size: 22.sp,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          chooseDestinationVIP(theaterAddress);
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              0.010.sw, 0.010.sh, 0.010.sw, 0.010.sh),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.deepPurpleAccent,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.3,
                                  offset: Offset(0.3, 0.3),
                                )
                              ]),
                          child: Icon(
                            (Icons.mic_external_on),
                            color: Colors.white,
                            size: 22.sp,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Future<void> getPlaceDirection(BuildContext context) async {
    var initialPos =
        Provider.of<AppData>(context, listen: false).pickUpLocation;
    var finalPos = Provider.of<AppData>(context, listen: false).dropOffLocation;

    var pickUpLatLng = LatLng(initialPos!.latitude, initialPos.longitude);
    var dropOffLatLng = LatLng(finalPos!.latitude, finalPos.longitude);

    showDialog(
        context: context,
        builder: (BuildContext context) => ProgressDialog(
              message: "Veuillez patienter...",
            ));

    var details = await AssistantMethods.obtainDirectionDetails(
        pickUpLatLng, dropOffLatLng);

    setState(() {
      tripDirectionDetails = details;
    });

    Navigator.pop(context);

    print("this is encoded points :: ");
    print(details!.encodedPoints);

    PolylinePoints polylinePoints = PolylinePoints();
    List<PointLatLng> decodedPolyLinePointsResult =
        polylinePoints.decodePolyline(details.encodedPoints!);

    if (decodedPolyLinePointsResult.isNotEmpty) {
      decodedPolyLinePointsResult.forEach((PointLatLng pointLatLng) {
        pLineCoordinates
            .add(LatLng(pointLatLng.latitude, pointLatLng.longitude));
      });
    }

    setState(() {
      Polyline polyline = Polyline(
          color: Colors.blueAccent,
          polylineId: const PolylineId("PolylineID"),
          jointType: JointType.round,
          points: pLineCoordinates,
          width: 5,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          geodesic: true);
      polylineSet.add(polyline);
    });
    LatLngBounds latLngBounds;
    if (pickUpLatLng.latitude > dropOffLatLng.latitude &&
        pickUpLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds =
          LatLngBounds(southwest: dropOffLatLng, northeast: pickUpLatLng);
    } else if (pickUpLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude),
          northeast: LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude));
    } else if (pickUpLatLng.latitude > dropOffLatLng.latitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude),
          northeast: LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude));
    } else {
      latLngBounds =
          LatLngBounds(southwest: pickUpLatLng, northeast: dropOffLatLng);
    }
    newGoogleMapController
        .animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));

    Marker pickUpLocMarker = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
      infoWindow:
          InfoWindow(title: initialPos.placeName, snippet: "Ma position"),
      position: pickUpLatLng,
      markerId: const MarkerId("pickUpId"),
    );
    Marker dropOffLocMarker = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      infoWindow:
          InfoWindow(title: finalPos.placeName, snippet: "Ma destination"),
      position: dropOffLatLng,
      markerId: const MarkerId("dropOffId"),
    );

    setState(() {
      markersSet.add(pickUpLocMarker);

      markersSet.add(dropOffLocMarker);
    });

    Circle pickUpLocCircle = Circle(
        fillColor: Colors.blueAccent,
        center: pickUpLatLng,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.blueAccent,
        circleId: const CircleId("pickUpId"));

    Circle dropOffLocCircle = Circle(
        fillColor: Colors.red,
        center: dropOffLatLng,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.red,
        circleId: const CircleId("dropOffId"));

    setState(() {
      circlesSet.add(pickUpLocCircle);
      circlesSet.add(dropOffLocCircle);
    });
  }

  Future<void> getPlaceDirectionPlace(BuildContext context) async {
    var initialPos =
        Provider.of<AppData>(context, listen: false).pickUpLocation;
    var finalPos = Provider.of<AppData>(context, listen: false).dropOffLocation;
    var initialClosestPos =
        Provider.of<AppData>(context, listen: false).closestPointPickUp;
    var finalClosestPos =
        Provider.of<AppData>(context, listen: false).closestPointDropOff;

    var pickUpLatLng = LatLng(initialPos!.latitude, initialPos.longitude);
    var dropOffLatLng = LatLng(finalPos!.latitude, finalPos.longitude);
    var pickUpLatLngClosest =
        LatLng(initialClosestPos!.latitude, initialClosestPos.longitude);
    var dropOffLatLngClosest =
        LatLng(finalClosestPos!.latitude, finalClosestPos.longitude);

    showDialog(
        context: context,
        builder: (BuildContext context) => ProgressDialog(
              message: "Veuillez patienter...",
            ));

    var details = await AssistantMethods.obtainDirectionDetails(
        pickUpLatLng, pickUpLatLngClosest);
    var details1 = await AssistantMethods.obtainDirectionDetails(
        pickUpLatLngClosest, dropOffLatLngClosest);
    var details2 = await AssistantMethods.obtainDirectionDetails(
        dropOffLatLngClosest, dropOffLatLng);

    setState(() {
      tripDirectionDetails = details;
    });

    Navigator.pop(context);

    print("this is encoded points :: ");
    print(details!.encodedPoints);

    PolylinePoints polylinePoints = PolylinePoints();
    List<PointLatLng> decodedPolyLinePointsResult =
        polylinePoints.decodePolyline(details.encodedPoints!);
    List<PointLatLng> decodedPolyLinePointsResult1 =
        polylinePoints.decodePolyline(details1!.encodedPoints!);
    List<PointLatLng> decodedPolyLinePointsResult2 =
        polylinePoints.decodePolyline(details2!.encodedPoints!);

    if (decodedPolyLinePointsResult.isNotEmpty) {
      decodedPolyLinePointsResult.forEach((PointLatLng pointLatLng) {
        pLineCoordinates
            .add(LatLng(pointLatLng.latitude, pointLatLng.longitude));
      });
    }
    if (decodedPolyLinePointsResult1.isNotEmpty) {
      decodedPolyLinePointsResult1.forEach((PointLatLng pointLatLng) {
        pLineCoordinates1
            .add(LatLng(pointLatLng.latitude, pointLatLng.longitude));
      });
    }
    if (decodedPolyLinePointsResult2.isNotEmpty) {
      decodedPolyLinePointsResult2.forEach((PointLatLng pointLatLng) {
        pLineCoordinates2
            .add(LatLng(pointLatLng.latitude, pointLatLng.longitude));
      });
    }

    setState(() {
      Polyline polyline = Polyline(
          color: Colors.orange,
          polylineId: const PolylineId("PolylineID"),
          jointType: JointType.round,
          points: pLineCoordinates,
          width: 5,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          geodesic: true);
      polylineSet.add(polyline);
    });
    setState(() {
      Polyline polyline = Polyline(
          color: Colors.blueAccent,
          polylineId: const PolylineId("PolylineID1"),
          jointType: JointType.round,
          points: pLineCoordinates1,
          width: 5,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          geodesic: true);
      polylineSet.add(polyline);
    });
    setState(() {
      Polyline polyline = Polyline(
          color: Colors.orange,
          polylineId: const PolylineId("PolylineID2"),
          jointType: JointType.round,
          points: pLineCoordinates2,
          width: 5,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          geodesic: true);
      polylineSet.add(polyline);
    });
    LatLngBounds latLngBounds;
    if (pickUpLatLng.latitude > dropOffLatLng.latitude &&
        pickUpLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds =
          LatLngBounds(southwest: dropOffLatLng, northeast: pickUpLatLng);
    } else if (pickUpLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude),
          northeast: LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude));
    } else if (pickUpLatLng.latitude > dropOffLatLng.latitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(dropOffLatLng.latitude, pickUpLatLng.longitude),
          northeast: LatLng(pickUpLatLng.latitude, dropOffLatLng.longitude));
    } else {
      latLngBounds =
          LatLngBounds(southwest: pickUpLatLng, northeast: dropOffLatLng);
    }
    newGoogleMapController
        .animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));
    Marker pickUpLocMarker = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
      infoWindow:
          InfoWindow(title: initialPos.placeName, snippet: "Ma position"),
      position: pickUpLatLng,
      markerId: const MarkerId("pickUpId"),
    );
    Marker dropOffLocMarker = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      infoWindow:
          InfoWindow(title: finalPos.placeName, snippet: "Ma destination"),
      position: dropOffLatLng,
      markerId: const MarkerId("dropOffId"),
    );
    Marker dropOffLocMarkerClosest = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
      infoWindow: InfoWindow(
          title: finalPos.placeName, snippet: "Ma destination Closest"),
      position: dropOffLatLngClosest,
      markerId: const MarkerId("dropOffClosestId"),
    );
    Marker pickUpLocMarkerClosest = Marker(
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
      infoWindow: InfoWindow(
          title: finalPos.placeName, snippet: "Ma destination Closest"),
      position: pickUpLatLngClosest,
      markerId: const MarkerId("pickUpClosestId"),
    );

    setState(() {
      markersSet.add(pickUpLocMarker);
      markersSet.add(pickUpLocMarkerClosest);
      markersSet.add(dropOffLocMarkerClosest);

      markersSet.add(dropOffLocMarker);
    });

    Circle pickUpLocCircle = Circle(
        fillColor: Colors.blueAccent,
        center: pickUpLatLng,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.blueAccent,
        circleId: const CircleId("pickUpId"));

    Circle dropOffLocCircle = Circle(
        fillColor: Colors.red,
        center: dropOffLatLng,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.red,
        circleId: const CircleId("dropOffId"));

    Circle dropOffLocClosestCircle = Circle(
        fillColor: Colors.orange,
        center: dropOffLatLngClosest,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.orange,
        circleId: const CircleId("dropOffClosestId"));

    Circle pickUpLocClosestCircle = Circle(
        fillColor: Colors.green,
        center: pickUpLatLngClosest,
        radius: 6,
        strokeWidth: 2,
        strokeColor: Colors.green,
        circleId: const CircleId("pickUpClosestId"));

    setState(() {
      circlesSet.add(pickUpLocCircle);
      circlesSet.add(dropOffLocCircle);
      circlesSet.add(dropOffLocClosestCircle);
      circlesSet.add(pickUpLocClosestCircle);
    });
  }

  void initGeoFireListener() {
    setState(() {
      initiatedDrivers = true;
    });
    GeoFireAssistant.nearByAvailableDriversList.clear();
    Geofire.initialize("availableDrivers");

    //comment
    Geofire.queryAtLocation(
            currentPosition.latitude, currentPosition.longitude, 15)!
        .listen((map) {
      if (map != null) {
        var callBack = map['callBack'];

        switch (callBack) {
          case Geofire.onKeyEntered:
            NearbyAvailableDrivers nearbyAvailableDrivers =
                NearbyAvailableDrivers();
            nearbyAvailableDrivers.key = map['key'];
            nearbyAvailableDrivers.latitude = map['latitude'];
            nearbyAvailableDrivers.longitude = map['longitude'];

            List nbrAv = GeoFireAssistant.nearByAvailableDriversList
                .where((element) => element.key == nearbyAvailableDrivers.key)
                .toList();
            if (nbrAv.isEmpty) {
              driverRef.child(nearbyAvailableDrivers.key!).once().then((value) {
                Map eventValue = value.snapshot.value as Map;
                if (!mounted) return;
                int? typeApp =
                    Provider.of<AppData>(context, listen: false).appType;
                print(typeApp.toString() + ' fzfzfzaaa');

                if (typeApp == 1) {
                  if (eventValue['mode'] == 'place') {
                    GeoFireAssistant.nearByAvailableDriversList
                        .add(nearbyAvailableDrivers);

                    GeoFireAssistant.nearByAvailableDriversList
                        .removeDuplicates(by: ((item) => item.key));

                    updateAvailableDriversOnMap();
                    addCloseTaxiTime();
                  }
                } else {
                  if (eventValue['mode'] == 'counter') {
                    GeoFireAssistant.nearByAvailableDriversList
                        .add(nearbyAvailableDrivers);
                    GeoFireAssistant.nearByAvailableDriversList
                        .removeDuplicates(by: ((item) => item.key));
                    updateAvailableDriversOnMap();
                    addCloseTaxiTime();
                  }
                }
              });
            }

            break;

          case Geofire.onKeyExited:
            GeoFireAssistant.removeDriversFromList(map['key']);
            updateAvailableDriversOnMap();
            addCloseTaxiTime();

            break;

          case Geofire.onKeyMoved:
            NearbyAvailableDrivers nearbyAvailableDrivers =
                NearbyAvailableDrivers();
            nearbyAvailableDrivers.key = map['key'];
            nearbyAvailableDrivers.latitude = map['latitude'];
            nearbyAvailableDrivers.longitude = map['longitude'];
            GeoFireAssistant.updateDriverNearbyLocation(nearbyAvailableDrivers);
            updateAvailableDriversOnMap();
            addCloseTaxiTime();

            break;

          case Geofire.onGeoQueryReady:
            updateAvailableDriversOnMap();
            addCloseTaxiTime();

            break;
        }
      }
      print(GeoFireAssistant.nearByAvailableDriversList.length.toString() +
          ' ffze');

      if (!mounted) return;

      setState(() {});
    });

    //comment
  }

  Future<void> addCloseTaxiTime() async {
    if (GeoFireAssistant.nearByAvailableDriversList.isNotEmpty) {
      var driver = GeoFireAssistant.nearByAvailableDriversList.first;
      LatLng userPosition =
          LatLng(currentPosition.latitude, currentPosition.longitude);
      LatLng driverPosition = LatLng(driver.latitude!, driver.longitude!);

      var details = await AssistantMethods.obtainDirectionDetails(
          userPosition, driverPosition);

      if (mounted) {
        setState(() {
          markersSet
              .addLabelMarker(LabelMarker(
            textStyle: TextStyle(
                fontSize: 30.sp,
                fontFamily: 'Jost',
                fontWeight: FontWeight.bold),
            label:
                '${(details!.durationValue! / 60).truncate().toString()} min',
            markerId: const MarkerId("driver"),
            position:
                LatLng(currentPosition.latitude, currentPosition.longitude),
            backgroundColor: Colors.deepPurpleAccent,
          ))
              .then(
            (value) {
              setState(() {});
            },
          );
        });
      }
    }
  }

  void updateAvailableDriversOnMap() {
    if (!mounted) return;

    setState(() {
      markersSet.clear();
    });

    Set<Marker> tMarkers = <Marker>{};
    for (NearbyAvailableDrivers driver
        in GeoFireAssistant.nearByAvailableDriversList) {
      LatLng driverAvailablePosition =
          LatLng(driver.latitude!, driver.longitude!);
      Marker marker = Marker(
          markerId: MarkerId('driver${driver.key}'),
          position: driverAvailablePosition,
          icon: nearByIcon!,
          rotation: AssistantMethods.createRandomNumber(360));
      tMarkers.add(marker);
    }

    setState(() {
      markersSet = tMarkers;
    });
    print(markersSet.length.toString() + ' fzfzfzvvvv');
  }

  void createIconMarker() {
    if (nearByIcon == null) {
      ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size(2.sp, 2.sp));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, "assets/images/car_icon_1.png")
          .then((value) {
        nearByIcon = value;
      });
    }
  }

  void cancelRideRequest() {
    // rideRequestRef.child('status').once().then((status) {
    //   String oldValue = status.snapshot.value.toString();
    //   if (oldValue != 'null') {
    //     rideRequestRef.child('status').set(oldValue + '<SPE>' + 'cancelled');
    //     rideRequestRef.remove();
    //   } else {
    //     rideRequestRef.child('status').set('cancelled');
    //
    //   }
    // });
    rideRequestRef.remove();
    setState(() {
      state = "normal";
    });
  }

  void searchNearestDriver() {
    if (availableDrivers!.isEmpty) {
      cancelRideRequest();
      resetApp();
      noDriverFound();
      return;
    }
    print(availableDrivers!.toString() + ' ml,nbd');
    var driver = availableDrivers!.first;
    notifyDriver(driver);
    availableDrivers!.removeAt(0);
    print(availableDrivers!.toString() + ' ml,nbd');
  }

  void noDriverFound() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => const NoDriverAvailableDialog());
  }

  void notifyDriver(NearbyAvailableDrivers driver) {
    driverRef.child(driver.key!).child("newRide").set(rideRequestRef.key);
    print(rideRequestRef.key!.toString() + ' mpoiuiyq');

    driverRef.child(driver.key!).child("token").once().then((snap) {
      if (snap.snapshot.value != null) {
        String token = snap.snapshot.value.toString();
        AssistantMethods.sendNotificationToDriver(
            token, context, rideRequestRef.key!);
      } else {
        return;
      }
      const oneSecondPassed = Duration(seconds: 1);
      var timer = Timer.periodic(oneSecondPassed, (timer) {
        if (state != "requesting") {
          driverRef.child(driver.key!).child("newRide").set("cancelled");
          driverRef.child(driver.key!).child("newRide").onDisconnect();
          driverRequestTimeOut = 40;
          timer.cancel();
        }
        driverRequestTimeOut = driverRequestTimeOut - 1;

        driverRef.child(driver.key!).child("newRide").onValue.listen((event) {
          if (event.snapshot.value.toString() == "accepted") {
            driverRef.child(driver.key!).child("newRide").onDisconnect();
            driverRequestTimeOut = 40;
            timer.cancel();
          }
        });
        if (driverRequestTimeOut == 0) {
          driverRef.child(driver.key!).child("newRide").set("timeout");
          driverRef.child(driver.key!).child("newRide").onDisconnect();
          driverRequestTimeOut = 40;
          timer.cancel();

          searchNearestDriver();
        }
      });
    });
  }

  Future<String?> getUsername() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userName = sharedPreferences.getString('userName');
    return userName;
  }

  Future<String?> getUserPhone() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? userPhone = sharedPreferences.getString('userPhone');
    return userPhone;
  }

  Future<int?> getUserId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    int? userId = sharedPreferences.getInt('userId');
    return userId;
  }

  void chooseDestinationWithPinConfirmation() async {
    if (cameraPosition != null && choosePin == true) {
      LatLng latLng = LatLng(
          cameraPosition!.target.latitude, cameraPosition!.target.longitude);
      List<Placemark> placeMarks = await GeocodingPlatform.instance
          .placemarkFromCoordinates(latLng.latitude, latLng.longitude);

      Address? address = Address();
      address.placeName = placeMarks[0].street!;
      address.placeId = '';
      address.latitude = latLng.latitude;
      address.longitude = latLng.longitude;
      Provider.of<AppData>(context, listen: false)
          .updateDropOffLocationAddress(address);

      if (GeoFireAssistant.nearByAvailableDriversList.isEmpty) {
        cancelRideRequest();
        resetApp();
        noDriverFound();
        return;
      }

      if (Provider.of<AppData>(context, listen: false).appType == 1) {
        var dropOff =
            Provider.of<AppData>(context, listen: false).dropOffLocation;
        var pickUp =
            Provider.of<AppData>(context, listen: false).pickUpLocation;

        var stations = Provider.of<AppData>(context, listen: false).stations;
        double pickUpStationDistance = Geolocator.distanceBetween(
            pickUp!.latitude,
            pickUp.longitude,
            stations[0].latitude!,
            stations[0].longitude!);
        double dropOffStationDistance = Geolocator.distanceBetween(
            dropOff!.latitude,
            dropOff.longitude,
            stations[0].latitude!,
            stations[0].longitude!);
        var pickUpStation = stations[0];
        var dropOffStation = stations[0];
        print('pickUpStationDistance 1: $pickUpStationDistance');
        print('dropOffStationDistance 1: $dropOffStationDistance');
        print('pickUpStation 1: $pickUpStation');
        print('dropOffStation 1: $dropOffStation');
        for (var station in stations) {
          double distanceInMetersPickUp = Geolocator.distanceBetween(
              pickUp.latitude,
              pickUp.longitude,
              station.latitude!,
              station.longitude!);
          if (distanceInMetersPickUp < pickUpStationDistance) {
            setState(() {
              pickUpStationDistance = distanceInMetersPickUp;
              pickUpStation = station;
            });
          }
          double distanceInMetersDropOff = Geolocator.distanceBetween(
              dropOff.latitude,
              dropOff.longitude,
              station.latitude!,
              station.longitude!);
          if (distanceInMetersDropOff < dropOffStationDistance) {
            setState(() {
              dropOffStationDistance = distanceInMetersDropOff;
              dropOffStation = station;
            });
          }
        }
        print('pickUpStationDistance 2: $pickUpStationDistance');
        print('dropOffStationDistance 2: $dropOffStationDistance');
        print('pickUpStation 2: $pickUpStation');
        print('dropOffStation 2: $dropOffStation');
        Provider.of<AppData>(context, listen: false)
            .updateClosestPickUpLocationAddress(
                Address(
                    placeName: pickUpStation.name!,
                    latitude: pickUpStation.latitude!,
                    longitude: pickUpStation.longitude!),
                pickUpStation);

        Provider.of<AppData>(context, listen: false)
            .updateClosestDropOffLocationAddress(
                Address(
                    placeName: dropOffStation.name!,
                    latitude: dropOffStation.latitude!,
                    longitude: dropOffStation.longitude!),
                dropOffStation);
        print(pickUpStation.idRoute);
        print(pickUpStation.order);
        print(dropOffStation.order);
        print("dadz");
        for (var nearbyDriver in GeoFireAssistant.nearByAvailableDriversList) {
          var details = await AssistantMethods.obtainDirectionDetails(
              LatLng(nearbyDriver.latitude!, nearbyDriver.longitude!),
              LatLng(
                  Provider.of<AppData>(context, listen: false)
                      .pickUpStation!
                      .latitude!,
                  Provider.of<AppData>(context, listen: false)
                      .pickUpStation!
                      .longitude!));
          setState(() {
            nearbyDriver.time = (details!.durationValue! / 60).truncate();
          });
        }
        AssistantMethods()
            .obtainPricePlace(
                Provider.of<AppData>(context, listen: false)
                    .pickUpStation!
                    .idRoute!,
                Provider.of<AppData>(context, listen: false)
                    .pickUpStation!
                    .order,
                Provider.of<AppData>(context, listen: false)
                    .dropOffStation!
                    .order)
            .then((value) {
          Provider.of<AppData>(context, listen: false).updatePlaceFee(value!);
        });
      } else {
        var driver = GeoFireAssistant.nearByAvailableDriversList.first;

        LatLng userPosition =
            LatLng(currentPosition.latitude, currentPosition.longitude);
        LatLng driverPosition = LatLng(driver.latitude!, driver.longitude!);
        var details = await AssistantMethods.obtainDirectionDetails(
            driverPosition, userPosition);

        var distance = details?.distanceValue!;
        var time = details?.durationValue!;
        setState(() {
          driverDistance = distance! / 1000;
          driverTime = time;
        });
      }

      setState(() {
        choosePin = false;
      });
      displayRideDetailsContainer();
    }
  }

  void chooseDestinationVIP(Address address) async {
    Provider.of<AppData>(context, listen: false)
        .updateDropOffLocationAddress(address);

    if (GeoFireAssistant.nearByAvailableDriversList.isEmpty) {
      cancelRideRequest();
      resetApp();
      noDriverFound();
      return;
    }
    var driver = GeoFireAssistant.nearByAvailableDriversList.first;

    LatLng userPosition =
        LatLng(currentPosition.latitude, currentPosition.longitude);
    LatLng driverPosition = LatLng(driver.latitude!, driver.longitude!);
    var details = await AssistantMethods.obtainDirectionDetails(
        driverPosition, userPosition);

    var distance = details?.distanceValue!;
    var time = details?.durationValue!;
    setState(() {
      driverDistance = distance! / 1000;
      driverTime = time;
    });
    setState(() {
      choosePin = false;
    });
    displayRideDetailsContainer();
  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }

  void getCameraPositionData() async {
    if (cameraPosition != null && choosePin == true) {
      try {
        LatLng latLng = LatLng(
            cameraPosition!.target.latitude, cameraPosition!.target.longitude);
        List<Placemark> placeMarks = await GeocodingPlatform.instance
            .placemarkFromCoordinates(latLng.latitude, latLng.longitude);
        print(placeMarks);
        setState(() {
          pinTitle = placeMarks[2].street.toString() +
              ", " +
              placeMarks[2].administrativeArea.toString() +
              ", " +
              placeMarks[2].country.toString();
        });
      } catch (e) {
        print(e);
      }
    }
  }

  void searchClosestPath() {
    double pickUpLatitude =
        Provider.of<AppData>(context, listen: false).pickUpLocation!.latitude;
    double pickUpLongitude =
        Provider.of<AppData>(context, listen: false).pickUpLocation!.longitude;
    double distance = 0;
    Address addressPath = Address();
    for (var path in pathList) {
      if (distance == 0) {
        setState(() {
          distance = Geolocator.distanceBetween(
              pickUpLatitude, pickUpLongitude, path.latitude, path.longitude);
          addressPath = path;
        });
      } else if (distance >
          Geolocator.distanceBetween(
              pickUpLatitude, pickUpLongitude, path.latitude, path.longitude)) {
        setState(() {
          distance = Geolocator.distanceBetween(
              pickUpLatitude, pickUpLongitude, path.latitude, path.longitude);
          addressPath = path;
        });
      }
    }

    Provider.of<AppData>(context, listen: false)
        .updateDropOffLocationAddress(addressPath);
    pathPredictionsList.addAll(
        pathList.where((element) => element.path == addressPath.path).toList());

    print(pathPredictionsList.toString() + 'mmll');
  }
}
