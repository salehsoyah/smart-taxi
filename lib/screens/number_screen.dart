import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/constant/colors.dart';


class NumberScreen extends StatefulWidget {
  NumberScreen({Key? key}) : super(key: key);
  static const String idScreen = "screennumber";

  @override
  State<NumberScreen> createState() => _NumberScreenState();
}

class _NumberScreenState extends State<NumberScreen> {
  final TextEditingController _phoneController = TextEditingController();

  late bool onClick;

  @override
  void initState() {
    // TODO: implement initState
    onClick = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Positioned.fill(
            child: SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Image.asset("assets/images/background/number_back.png",
                  fit: BoxFit.cover),
            ),
          ),
          Positioned.fill(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white.withOpacity(0.5),
            ),
          ),
          Positioned(
            top: 0.2.sw,
            bottom: 0.sw,
            right: 0.1.sw,
            left: 0.1.sw,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 200,
                    height: 60,
                    child: Image.asset("assets/images/HELLO.png",
                        fit: BoxFit.fitWidth),
                  ),
                  SizedBox(
                    width: 200,
                    height: 60,
                    child: Image.asset("assets/images/HELLO1.png",
                        fit: BoxFit.fitWidth),
                  ),
                  const SizedBox(
                    //height: 0.1.sw,
                    height: 10,
                  ),
                  Text(
                    "C'EST QUOI \n VOTRE NUMERO ? ",
                    maxLines: 2,
                    style: GoogleFonts.cairo(
                      fontSize: 26.sp,
                      //letterSpacing: 1,
                      fontWeight: FontWeight.bold,
                      color: black,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "عسلامة !",
                        style: GoogleFonts.cairo(
                          fontSize: 26.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.end,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                height: 35,
                                child: Image.asset("assets/images/tnflag.png",
                                    fit: BoxFit.fitWidth),
                              ),
                              Text(
                                " + 216",
                                style: GoogleFonts.cairo(
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                                textAlign: TextAlign.end,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: 150,
                            height: 1,
                            color: Colors.grey,
                          ),
                        ],
                      ),
                      const SizedBox(width: 25),
                      SizedBox(
                        height: 55,
                        width: 135,
                        child: TextField(
                          controller: _phoneController,
                          keyboardType: TextInputType.phone,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            labelText: "NUMERO DE TELEPHONE",
                            labelStyle: GoogleFonts.cairo(
                                fontSize: 11.0.sp,
                                color: Colors.grey[600],
                                fontWeight: FontWeight.bold),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 10.0.sp,
                            ),
                          ),
                          style: TextStyle(
                              fontSize: 19.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 0.1.sw,
                  ),
                  Center(
                    child: SizedBox(
                      width: 120,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            textStyle: const TextStyle(color: Colors.black),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24.0))),
                        onPressed: ()async {
                          if (_phoneController.text.isEmpty) {
                            displayToastMessage(
                                "Entrer votre numéro.", context);
                          } else if (_phoneController.text.length < 8 ||
                              _phoneController.text.length > 8) {
                            displayToastMessage(
                                "Vérifier votre numéro.", context);
                          } else {
                            FocusScope.of(context).unfocus();
                            setState(() {
                              onClick = true;
                            });
                             UserMethods.loginUserByPhone(_phoneController.text.toString(), context);
                            Future.delayed(
                              const Duration(seconds: 1,),
                                  () {
                                    setState(() {
                                      onClick = false;
                                    });
                              },
                            );
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                            0.01.sw,
                            0.01.sh,
                            0.01.sw,
                            0.01.sh,
                          ),
                          child: const Center(
                            child: Text(
                              "Confirmer",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14.0,
                                fontFamily: 'Jost',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          onClick
              ? Positioned(
                  bottom: 0.1.sw,
                  right: 0.1.sw,
                  left: 0.1.sw,
                  child: const LinearProgressIndicator(
                    color: Colors.red,
                    backgroundColor: Colors.grey,
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
}
