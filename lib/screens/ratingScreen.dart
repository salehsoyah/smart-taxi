import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:taxi_finder/config_maps.dart';

class RatingDialog extends StatefulWidget {
  const RatingDialog({Key? key, required this.driverId}) : super(key: key);
  final String driverId;

  @override
  State<RatingDialog> createState() => _RatingDialogState();
}

class _RatingDialogState extends State<RatingDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        backgroundColor: Colors.orange,
        child: Container(
          margin: EdgeInsets.all(7.0.sp),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 22.0.sp,
              ),
              Text(
                "Noté ce chauffeur",
                style: TextStyle(
                    fontFamily: 'Jost',
                    fontWeight: FontWeight.bold,
                    fontSize: 15.sp),
              ),
              const Divider(
                height: 2.0,
                thickness: 2.0,
              ),
              SizedBox(
                height: 16.0.sp,
              ),
              const SizedBox(
                height: 16.0,
              ),
              RatingBar.builder(
                itemSize: 35.sp,
                initialRating: starCounter,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0.sp),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  starCounter = rating;
                  if (starCounter.truncate() == 1) {
                    setState(() {
                      title = "Trés Mauvais";
                    });
                  }
                  if (starCounter.truncate() == 2) {
                    setState(() {
                      title = "Mauvais";
                    });
                  }
                  if (starCounter.truncate() == 3) {
                    setState(() {
                      title = "Bien";
                    });
                  }
                  if (starCounter.truncate() == 4) {
                    setState(() {
                      title = "Trés Bien";
                    });
                  }
                  if (starCounter.truncate() == 5) {
                    setState(() {
                      title = "Excellent";
                    });
                  }
                },
              ),
              SizedBox(
                height: 14.0.sp,
              ),
              Text(
                title,
                style: TextStyle(
                    fontSize: 50.0.sp,
                    fontFamily: "Jost",
                    color: Colors.orange),
              ),
              SizedBox(
                height: 16.0.sp,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0.sp),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.orange),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24.0),
                      ),
                    ),
                  ),
                  onPressed: () async {
                    DatabaseReference driverRatingRef = FirebaseDatabase
                        .instance
                        .ref()
                        .child("drivers")
                        .child(widget.driverId)
                        .child("ratings");
                    driverRatingRef.once().then((value) {
                      if (value.snapshot.value != null) {
                        double oldRatings =
                            double.parse(value.snapshot.value.toString());
                        double addRating = oldRatings + starCounter;
                        double averageRating = addRating / 2;
                        driverRatingRef.set(averageRating.toString());
                      } else {
                        driverRatingRef.set(starCounter.toString());
                      }
                    });
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(13.0.sp),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Noté",
                          style: TextStyle(
                              fontSize: 20.0.sp,
                              fontFamily: "Jost",
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Icon(
                          FontAwesomeIcons.star,
                          color: Colors.white,
                          size: 22.0.sp,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
