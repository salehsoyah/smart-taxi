import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/main.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/screens/register_screen.dart';
import 'package:taxi_finder/widgets/progress_dialog.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  static const String idScreen = "login";

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(
            0.02.sw,
            0.02.sh,
            0.02.sw,
            0.02.sh,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 0.2.sh,
              ),
              Container(
                width: 0.3.sw,
                height: 0.15.sh,
                padding: EdgeInsets.fromLTRB(
                  0.02.sw,
                  0.02.sh,
                  0.02.sw,
                  0.02.sh,
                ),
                decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(15)),
                child: const Center(
                  child: Image(
                    image: AssetImage("assets/images/logo_app.png"),
                  ),
                ),
              ),
              SizedBox(
                height: 0.04.sh,
              ),
              Text(
                "Entrez vos informations",
                style: TextStyle(
                    fontSize: 24.0.sp,
                    fontFamily: 'Jost',
                    fontWeight: FontWeight.w900),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  0.06.sw,
                  0.02.sh,
                  0.06.sw,
                  0.02.sh,
                ),
                child: Column(
                  children: [
                    TextField(
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration:  InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle:
                          TextStyle(color: Colors.grey, fontSize: 16.0.sp,)),
                      style:  TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),

                    TextField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration:  InputDecoration(
                          labelText: "Password",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle:
                          TextStyle(color: Colors.grey, fontSize: 16.0.sp,)),
                      style:  TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                        height: 0.02.sh
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.orange,
                          textStyle: const TextStyle(color: Colors.white),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24.0))),
                      onPressed: () {
                        if (!_emailController.text.contains("@")) {
                          displayToastMessage(
                              "l'adresse email n'est pas valide", context);
                        } else if (_passwordController.text.isEmpty) {
                          displayToastMessage(
                              "Le mot de passe est obligatoire.", context);
                        } else {
                          UserMethods.loginUser(_emailController.text, _passwordController.text, context);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(
                          0.01.sw,
                          0.01.sh,
                          0.01.sw,
                          0.01.sh,
                        ),
                        child: const Center(
                          child: Text(
                            "Login",
                            style: TextStyle(
                                fontSize: 22.0,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, RegisterScreen.idScreen, (route) => false);
                  },
                  child: const Text(
                    "Vous n'avez pas de compte? Inscrivez-vous ici",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Jost',
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  // void loginUser(BuildContext context) async {
  //   showDialog(
  //     context: context,
  //     barrierDismissible: false,
  //     builder: (BuildContext context) =>
  //         ProgressDialog(message: "Authentification, veuillez patienter ..."),
  //   );
  //
  //   final User? user = (await FirebaseAuth.instance
  //           .signInWithEmailAndPassword(
  //               email: _emailController.text,
  //               password: _passwordController.text)
  //           .catchError((errMsg) {
  //     Navigator.pop(context);
  //     displayToastMessage("Erreur: " + errMsg.toString(), context);
  //   }))
  //       .user;
  //
  //   if (user != null) {
  //     userRef.child(user.uid).once().then((value) => (DataSnapshot snap) {
  //           if (snap.value != null) {
  //             Navigator.pushNamedAndRemoveUntil(
  //                 context, MainScreen.idScreen, (route) => false);
  //             displayToastMessage("Vous êtes connecté maintenant.", context);
  //           } else {
  //             Navigator.pop(context);
  //             FirebaseAuth.instance.signOut();
  //             displayToastMessage(
  //                 "Aucun enregistrement n'existe pour cet utilisateur. Veuillez créer un nouveau compte.",
  //                 context);
  //           }
  //         });
  //
  //     Navigator.pushNamedAndRemoveUntil(
  //         context, MainScreen.idScreen, (route) => false);
  //   } else {
  //     Navigator.pop(context);
  //     displayToastMessage("Une erreur s'est produite, impossible de se connecter.", context);
  //   }
  // }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
}
