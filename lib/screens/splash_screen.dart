import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:taxi_finder/screens/login_screen.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/screens/number_screen.dart';
import 'package:taxi_finder/screens/ride_options.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);
  static const String idScreen = "splash";

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 3), () {
     print( FirebaseAuth.instance.currentUser);
      FirebaseAuth.instance.currentUser == null
          ? Navigator.push(
              context,
          //    MaterialPageRoute(builder: (context) => LoginScreen()),
        MaterialPageRoute(builder: (context) => NumberScreen()),
            )
          : Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>  MainScreen(type: '2',)));
    });
    return Scaffold(
     body: Stack(
        children:[
          Positioned.fill(
         child:SizedBox(
         width:double.infinity,
        height:double.infinity,
        child:Image.asset("assets/images/background/splash_back.png",
       fit:BoxFit.fill )
     )
    ),
          Positioned(
            top: 0.sw,
            bottom: 0.sw,
            right: 0.sw,
            left: 0.sw,
            child: SizedBox(
              width: 75,
              height: 75,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/background/logo_inno.png"),
                  SizedBox(height: 0.01.sw),

                  ///Text('Please check your internet connection!')
                  SizedBox(
                    height: 50.sp,
                    width: 50.sp,
                    child: const CircularProgressIndicator(
                      backgroundColor: Colors.grey,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
     ),
    );
  }
}