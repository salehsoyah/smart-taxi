import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/assistants/assistant_methods.dart';
import 'package:taxi_finder/assistants/geo_fire_assistant.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/screens/trajets.dart';

class RideOptions extends StatefulWidget {
  static const String idScreen = "optionsScreen";

  const RideOptions({Key? key}) : super(key: key);

  @override
  State<RideOptions> createState() => _RideOptionsState();
}

class _RideOptionsState extends State<RideOptions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0.08.sh,
        backgroundColor: Colors.orange,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.fromLTRB(0.04.sw, 0.03.sh, 0.04.sw, 0.03.sh),
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.01.sw, 0.01.sh, 0.01.sw, 0.01.sh),
                  child: Container(
                    width: 0.5.sw,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.local_taxi_sharp,
                          size: 30.sp,
                        ),
                        Text(
                          'Taxi par place',
                          style: TextStyle(
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp,
                          overflow: TextOverflow.ellipsis),
                        ),
                      ],
                    ),
                  ),
                ),
                onPressed: () {
                  setState(() {
                    GeoFireAssistant.nearByAvailableDriversList.clear();
                  });
                  Provider.of<AppData>(context, listen: false).updateAppType(1);
                  AssistantMethods.obtainRoutes().then((value) {
                    Provider.of<AppData>(context, listen: false).updateStationsList(value!);
                    print(Provider.of<AppData>(context, listen: false).stations.toString() + ' fzzfgggg');
                 /*   Navigator.push(
                        context,
                        MaterialPageRoute(
                        //    builder: (context) =>  MainScreen(type: '1',)));
                            builder: (context) =>Trajets()));*/
                    AssistantMethods.obtainRoutess().then((value) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Trajets(routes: value!)));
                    });
                  });

                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    textStyle: const TextStyle(color: Colors.white),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0))),
              ),
              SizedBox(height: 7.sp,),
              ElevatedButton(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.01.sw, 0.01.sh, 0.01.sw, 0.01.sh),
                  child: Container(
                    width: 0.5.sw,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.local_taxi_sharp,
                          size: 30.sp,
                        ),
                        Text(
                          'Taxi Compteur',
                          style: TextStyle(
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp),
                        ),
                      ],
                    ),
                  ),
                ),
                onPressed: () {
                  setState(() {
                    GeoFireAssistant.nearByAvailableDriversList.clear();
                  });
                  Provider.of<AppData>(context, listen: false).updateAppType(2);

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>  MainScreen(type: '2',)));
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    textStyle: const TextStyle(color: Colors.white),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0))),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
