import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:taxi_finder/assistants/user_methods.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:taxi_finder/main.dart';
import 'package:taxi_finder/models/user.dart';
import 'package:taxi_finder/screens/login_screen.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/widgets/progress_dialog.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  static const String idScreen = "register";

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _firstNameController = TextEditingController();

  final TextEditingController _familyNameController = TextEditingController();

  final TextEditingController _cinController = TextEditingController();

  final TextEditingController _addressController = TextEditingController();

  final TextEditingController _phoneController = TextEditingController();

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  File? pictureFile;

  String? pictureConverted;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffB9B0A2),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(
            0.02.sw,
            0.02.sh,
            0.02.sw,
            0.02.sh,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 0.2.sh,
              ),
              GestureDetector(
                onTap: () async {
                  if (pictureFile == null) {
                    FilePickerResult? result =
                        await FilePicker.platform.pickFiles();

                    if (result != null) {
                      setState(() {
                        pictureFile = File(result.files.single.path!);
                        image2Base64(pictureFile!.path).then((value) {
                          setState(() {
                            pictureConverted = value;
                          });
                        });
                      });
                    } else {
                      // User canceled the picker
                    }
                  }
                },
                child: Container(
                  width: 0.3.sw,
                  height: 0.15.sh,
                  decoration: const BoxDecoration(
                      color: Colors.orange,
                      shape: BoxShape.circle,
                     ),
                  child: const Center(
                    child: Icon(Icons.person_outline_rounded, size: 80,)
                  ),
                ),
              ),
              SizedBox(
                height: 0.01.sh,
              ),
              Text(
                "Choisissez une photo de profil",
                style: TextStyle(
                    fontSize: 16.0.sp,
                    fontFamily: 'Jost',
                    fontWeight: FontWeight.bold, color: Colors.grey),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 0.04.sh,
              ),
              Text(
                "Entrez vos informations",
                style: TextStyle(
                    fontSize: 28.0.sp,
                    fontFamily: 'Jost',
                    fontWeight: FontWeight.w900),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  0.06.sw,
                  0.02.sh,
                  0.06.sw,
                  0.02.sh,
                ),
                child: Column(
                  children: [
                    TextField(
                      controller: _firstNameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: "Prénom",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle:
                          TextStyle(color: Colors.grey, fontSize: 16.0.sp)),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _familyNameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: "Nom",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _phoneController,
                      maxLength: 8,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          labelText: "Téléphone",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _cinController,
                      keyboardType: TextInputType.number,
                      maxLength: 8,
                      decoration: InputDecoration(
                          labelText: "CIN",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _addressController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: "Adresse",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: "Email",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontFamily: 'Jost',
                          fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          labelText: "Password",
                          labelStyle: TextStyle(
                              fontSize: 16.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0.sp,
                          )),
                      style: TextStyle(
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Jost'),
                    ),
                    SizedBox(
                      height: 0.02.sh,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.orange,
                          textStyle: const TextStyle(color: Colors.white),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(24.0))),
                      onPressed: () {
                        if (_firstNameController.text.length < 3) {
                          displayToastMessage(
                              "Le Prénom doit comporter au moins 3 caractères.",
                              context);
                        }
                        else if (_familyNameController.text.length < 3) {
                          displayToastMessage(
                              "Le nom doit comporter au moins 3 caractères.",
                              context);
                        }
                        else if (_phoneController.text.length != 8) {
                          displayToastMessage(
                              "Le numéro de téléphone doit être 8 nombres.",
                              context);
                        }
                        else if (_cinController.text.length != 8) {
                          displayToastMessage(
                              "Le CIN de téléphone doit être 8 nombres.",
                              context);
                        }
                        else if (_addressController.text.length < 3) {
                          displayToastMessage(
                              "L'addresse doit comporter au moins 3 caractères.",
                              context);
                        }
                        else if (_emailController.text.isEmpty) {
                          displayToastMessage(
                              "l'adresse mail est obligatoire",
                              context);
                        }
                        else if (!RegExp(
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                            .hasMatch(_emailController.text)) {
                          displayToastMessage(
                              "l'adresse mail n'est pas valide", context);
                        }
                        else if (_passwordController.text.length < 8) {
                          displayToastMessage(
                              "Le mot de passe doit être au moin 8 charactères.",
                              context);
                        }
                        else if (pictureConverted == null) {
                          displayToastMessage(
                              "La photo de profil est obligatoire",
                              context);
                        }
                        else {
                          User user = User();
                          user.firstName = _firstNameController.text;
                          user.familyName = _familyNameController.text;
                          user.phone = _phoneController.text;
                          user.cin = _cinController.text;
                          user.address = _addressController.text;
                          user.email = _emailController.text;
                          user.password = _passwordController.text;
                          user.photo = pictureConverted;

                          UserMethods.registerUser(user, context);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(
                          0.01.sw,
                          0.01.sh,
                          0.01.sw,
                          0.01.sh,
                        ),
                        child: Center(
                          child: Text(
                            "Créer un compte",
                            style: TextStyle(
                                fontSize: 20.0.sp,
                                fontFamily: 'Jost',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, LoginScreen.idScreen, (route) => false);
                  },
                  child:  Text(
                    "Vous avez déjà un compte? Connectez-vous ici",
                    style: TextStyle(
                        fontSize: 14.0.sp,
                        fontFamily: 'Jost',
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  // void registerNewUser(BuildContext context) async {
  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
  static Future image2Base64(String path) async {
    File file = File(path);
    List<int> imageBytes = await file.readAsBytes();
    return base64Encode(imageBytes);
  }
}
