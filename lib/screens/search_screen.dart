import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/assistants/assistant_methods.dart';
import 'package:taxi_finder/assistants/request_assistant.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/database/locations_database.dart';
import 'package:taxi_finder/models/address.dart';
import 'package:taxi_finder/models/place_predictions.dart';
import 'package:taxi_finder/widgets/divider.dart';
import 'package:taxi_finder/widgets/progress_dialog.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen({Key? key, required this.type}) : super(key: key);
  String type;

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final TextEditingController pickUpController = TextEditingController();
  final TextEditingController dropOffController = TextEditingController();

  List<dynamic> placePredictionsList = [];

  @override
  void dispose() {
    pickUpController.dispose();
    dropOffController.dispose();
    placePredictionsList.clear();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (pickUpController.text.isEmpty) {
      if (Provider
          .of<AppData>(context)
          .pickUpLocation != null) {
        String placeAddress =
            Provider
                .of<AppData>(context)
                .pickUpLocation!
                .placeName;
        pickUpController.text = placeAddress;
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        toolbarHeight: 0.08.sh,
        backgroundColor: Colors.orange,
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 2.0,
                      spreadRadius: 0.3,
                      offset: Offset(0.3, 0.3),
                    )
                  ]),
              child: Padding(
                padding:
                EdgeInsets.fromLTRB(0.03.sw, 0.025.sh, 0.03.sw, 0.025.sh),
                child: Column(
                  children: [
                    SizedBox(height: 0.02.sh),
                    Row(
                      children: [
                        Icon(
                          Icons.location_pin,
                          color: Colors.blue,
                          size: 25.sp,
                        ),
                        SizedBox(width: 0.02.sw),
                        Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                    0.03.sw, 0.005.sh, 0.03.sw, 0.005.sh),
                                child: TextField(
                                  enabled: widget.type == '1' ? false : true,
                                  onChanged: (val) {
                                    findPlaces(val, 1);
                                  },
                                  controller: pickUpController,
                                  style: TextStyle(
                                      fontSize: 16.0.sp,
                                      fontFamily: "Jost",
                                      fontWeight: FontWeight.bold),
                                  decoration: InputDecoration(
                                    hintText: "Location",
                                    labelStyle: TextStyle(
                                        fontSize: 16.0.sp,
                                        fontFamily: "Jost",
                                        fontWeight: FontWeight.bold),
                                    hintStyle: TextStyle(
                                        fontSize: 16.0.sp,
                                        fontFamily: "Jost",
                                        fontWeight: FontWeight.bold),
                                    fillColor: Colors.white,
                                    filled: true,
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    isDense: true,
                                    contentPadding: EdgeInsets.fromLTRB(
                                        0.03.sw, 0.01.sh, 0.03.sw, 0.01.sh),
                                  ),
                                ),
                              ),
                            ))
                      ],
                    ),
                    SizedBox(height: 0.02.sh),
                    Row(
                      children: [
                        Icon(
                          Icons.location_pin,
                          color: Colors.red,
                          size: 25.sp,
                        ),
                        SizedBox(width: 0.02.sw),
                        Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                    0.03.sw, 0.005.sh, 0.03.sw, 0.005.sh),
                                child: TextField(
                                  onChanged: (val) {
                                    findPlaces(val, 2);
                                  },
                                  controller: dropOffController,
                                  style: TextStyle(
                                      fontSize: 16.0.sp,
                                      fontFamily: "Jost",
                                      fontWeight: FontWeight.bold),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    hintText: "Où allez-vous ?",
                                    hintStyle: TextStyle(
                                        fontSize: 16.0.sp,
                                        fontFamily: "Jost",
                                        fontWeight: FontWeight.bold),
                                    fillColor: Colors.white,
                                    filled: true,
                                    isDense: true,
                                    contentPadding: EdgeInsets.fromLTRB(
                                        0.03.sw, 0.01.sh, 0.03.sw, 0.01.sh),
                                  ),
                                ),
                              ),
                            ))
                      ],
                    )
                  ],
                ),
              ),
            ),
            placePredictionsList.isEmpty
                ? Expanded(
              child: FutureBuilder<List<dynamic>>(
                future: LocationHelper.instance.getPlaces(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<dynamic>> snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  }
                  return ListView(
                    children: snapshot.data!.map((e) {
                      return Padding(
                          padding: EdgeInsets.fromLTRB(
                              0.03.sw, 0.01.sh, 0.03.sw, 0.01.sh),
                          child: Row(
                            children: [
                              Expanded(
                                child: PredictionTile(
                                  placePredictions: PlacePrediction(
                                      secondaryText: e['secondaryText'],
                                      mainText: e['mainText'],
                                      placeId: e['placeId'],
                                      type: e['type']),
                                  dropOffController: dropOffController,
                                  pickUpController: pickUpController,
                                  goBack: pickUpController.text != ''
                                      ? true
                                      : false,
                                  type: '2',
                                ),
                              ),
                              IconButton(
                                  onPressed: () async {
                                    setState(() {
                                      LocationHelper.instance
                                          .remove(e['id']);
                                    });
                                  },
                                  icon: const Icon(Icons.close))
                            ],
                          ));
                    }).toList(),
                  );
                },
              ),
            )
                : Container(),
            (placePredictionsList.isNotEmpty)
                ? Padding(
              padding: EdgeInsets.symmetric(
                vertical: 0.015.sh,
                horizontal: 0.04.sw,
              ),
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return PredictionTile(
                    placePredictions: placePredictionsList[index],
                    goBack: (pickUpController.text != '' &&
                        dropOffController.text != '')
                        ? true
                        : false,
                    pickUpController: pickUpController,
                    dropOffController: dropOffController,
                    type: '2',
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                const DividerWidget(),
                itemCount: placePredictionsList.length,
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
              ),
            )
                : Container(),
            // (widget.pathPredictionsList!.isNotEmpty)
            //     ? Padding(
            //         padding: EdgeInsets.symmetric(
            //           vertical: 0.015.sh,
            //           horizontal: 0.04.sw,
            //         ),
            //         child: ListView.separated(
            //           itemBuilder: (context, index) {
            //             return PredictionTile(
            //               placePredictions: PlacePrediction(
            //                   secondaryText:
            //                   widget.pathPredictionsList![index].placeName,
            //                   mainText: widget.pathPredictionsList![index].placeName,
            //                   placeId: '',
            //                   type: 2),
            //               goBack: true,
            //               pickUpController: pickUpController,
            //               dropOffController: dropOffController,
            //               type: '1',
            //             );
            //           },
            //           separatorBuilder: (BuildContext context, int index) =>
            //               const DividerWidget(),
            //           itemCount: widget.pathPredictionsList!.length,
            //           shrinkWrap: true,
            //           physics: const ClampingScrollPhysics(),
            //         ),
            //       )
            //     : Container(),
            Expanded(child: Container()),
            Align(
              alignment: Alignment.bottomCenter,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    textStyle: const TextStyle(color: Colors.white),
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0)))),
                onPressed: () {
                  Navigator.pop(context, "pin");
                },
                child: SizedBox(
                  height: 0.07.sh,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/pin.png",
                          width: 0.06.sw,
                          height: 0.04.sh,
                        ),
                        SizedBox(
                          width: 0.003.sw,
                        ),
                        Text(
                          "Choisir sur la carte",
                          style: TextStyle(
                              fontSize: 22.0.sp,
                              fontFamily: 'Jost',
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void findPlaces(String placeName, int type) async {
    if (placeName.length > 1) {
      String autoCompleteUrl =
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$placeName&key=$mapKey&sessionToken=1234567890&components=country:tn";

      try {
        var res = await RequestAssistant.getRequest(Uri.parse(autoCompleteUrl));
        if (res == "not failed.") {
          return;
        }

        if (res["status"] == "OK") {
          var predictions = res["predictions"];
          setState(() {
            placePredictionsList = predictions
                .map((e) => PlacePrediction.fromJson(e, type))
                .toList();
          });
        }
      } catch (e) {
        print("" + e.toString());
      }
    }
  }
}

class PredictionTile extends StatefulWidget {
  final PlacePrediction placePredictions;
  final bool goBack;
  final String type;
  final TextEditingController pickUpController;
  final TextEditingController dropOffController;

  const PredictionTile({Key? key,
    required this.placePredictions,
    required this.goBack,
    required this.pickUpController,
    required this.dropOffController,
    required this.type})
      : super(key: key);

  @override
  State<PredictionTile> createState() => _PredictionTileState();
}

class _PredictionTileState extends State<PredictionTile> {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        getPlaceAddressDetails(widget.placePredictions.placeId, context,
            widget.pickUpController, widget.dropOffController);
      },
      child: Column(
        children: [
          Row(
            children: [
              Icon(
                Icons.location_pin,
                color: Colors.red,
                size: 25.sp,
              ),
              SizedBox(
                width: 0.02.sw,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.placePredictions.mainText,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 14.0.sp,
                          fontFamily: "Jost",
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 0.002.sh,
                    ),
                    Text(
                      widget.placePredictions.secondaryText,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 12.0.sp,
                          fontFamily: "Jost",
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            width: 0.002.sw,
          ),
        ],
      ),
    );
  }

  void getPlaceAddressDetails(String placeId,
      context,
      TextEditingController pickUpController,
      TextEditingController dropOffController) async {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            ProgressDialog(
              message: "Veuillez patienter...",
            ));
    String placeDetailsUrl =
        "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$mapKey";

    var res = await RequestAssistant.getRequest(Uri.parse(placeDetailsUrl));
    Navigator.pop(context);
    if (res == "Failed.") {
      return;
    }
    if (res["status"] == "OK") {
      Address? address = Address();
      address.placeName = res['result']['name'];
      address.placeId = placeId;
      address.latitude = res['result']['geometry']['location']['lat'];
      address.longitude = res['result']['geometry']['location']['lng'];

      pickUpController.clear();
      dropOffController.clear();
      if (widget.placePredictions.type == 1) {
        Provider.of<AppData>(context, listen: false)
            .updatePickUpLocationAddress(address);
      } else {
        setState(() {
          dropOffController.text = res['result']['name'];
        });
        Provider.of<AppData>(context, listen: false)
            .updateDropOffLocationAddress(address);

        if (Provider
            .of<AppData>(context, listen: false)
            .appType == 1) {
          var dropOff =
              Provider
                  .of<AppData>(context, listen: false)
                  .dropOffLocation;
          var pickUp =
              Provider
                  .of<AppData>(context, listen: false)
                  .pickUpLocation;

          var stations = Provider
              .of<AppData>(context, listen: false)
              .stations;
          double pickUpStationDistance = Geolocator.distanceBetween(
              pickUp!.latitude,
              pickUp.longitude,
              stations[0].latitude!,
              stations[0].longitude!);
          double dropOffStationDistance = Geolocator.distanceBetween(
              dropOff!.latitude,
              dropOff.longitude,
              stations[0].latitude!,
              stations[0].longitude!);
          var pickUpStation = stations[0];
          var dropOffStation = stations[0];
          print('pickUpStationDistance 1: $pickUpStationDistance');
          print('dropOffStationDistance 1: $dropOffStationDistance');
          print('pickUpStation 1: $pickUpStation');
          print('dropOffStation 1: $dropOffStation');
          for (var station in stations) {
            double distanceInMetersPickUp = Geolocator.distanceBetween(
                pickUp.latitude,
                pickUp.longitude,
                station.latitude!,
                station.longitude!);
            if (distanceInMetersPickUp < pickUpStationDistance) {
              setState(() {
                pickUpStationDistance = distanceInMetersPickUp;
                pickUpStation = station;
              });
            }
            double distanceInMetersDropOff = Geolocator.distanceBetween(
                dropOff.latitude,
                dropOff.longitude,
                station.latitude!,
                station.longitude!);
            if (distanceInMetersDropOff < dropOffStationDistance) {
              setState(() {
                dropOffStationDistance = distanceInMetersDropOff;
                dropOffStation = station;
              });
            }
          }
          print('pickUpStationDistance 2: $pickUpStationDistance');
          print('dropOffStationDistance 2: $dropOffStationDistance');
          print('pickUpStation 2: $pickUpStation');
          print('dropOffStation 2: $dropOffStation');
          Provider.of<AppData>(context, listen: false)
              .updateClosestPickUpLocationAddress(Address(
              placeName: pickUpStation.name!,
              latitude: pickUpStation.latitude!,
              longitude: pickUpStation.longitude!), pickUpStation);

          Provider.of<AppData>(context, listen: false)
              .updateClosestDropOffLocationAddress(Address(
              placeName: dropOffStation.name!,
              latitude: dropOffStation.latitude!,
              longitude: dropOffStation.longitude!), dropOffStation);
            print(pickUpStation.idRoute);
          print(pickUpStation.order);
          print(dropOffStation.order);
          print("dadz");

        }


        await LocationHelper.instance.add(widget.placePredictions);
      }

      if (widget.goBack) {
        Navigator.pop(context, true);
      }
    }
  }
}
