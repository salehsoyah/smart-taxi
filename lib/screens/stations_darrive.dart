import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:taxi_finder/models/station.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:taxi_finder/screens/trajets.dart';
import 'package:taxi_finder/widgets/message_dialog.dart';

class Stations_arrive extends StatefulWidget {
  Stations_arrive({Key? key, required this.stations,required this.idstationdepart}) : super(key: key);
  static const String idScreen = "pathScreen";
  List<Station> stations;
  int idstationdepart ;

  @override
  _Stations_arriveState createState() => _Stations_arriveState();
}

class _Stations_arriveState extends State<Stations_arrive> {
  int path = -1;
  String? title = 'Route de Sidi Mansour';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffB9B0A2),
      appBar: AppBar(
        toolbarHeight: 0.08.sh,
        backgroundColor: Color(0xffB9B0A2),
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding:
            EdgeInsets.only(left: 0.02.sw, bottom: 0.009.sh, top: 0.01.sh),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Choisissez une station d'arrivée",
                 style:TextStyle(
                    fontSize: 25.sp,
                    fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: widget.stations.length,
                itemBuilder: (BuildContext context, int index) {
                if   (index!=widget.idstationdepart) {
                  return
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 26.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color:Colors.grey,
                              ),

                              child: RadioListTile(
                                activeColor: Color(0xffB9B0A2),
                                title: Text(
                                 widget.stations[index].name!,
                                  style: TextStyle(
                                      fontFamily: 'Jost',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.sp),
                                ),
                                value: index,
                                groupValue: path,
                                onChanged: (int? value) {
                                  setState(() {
                                    path = value!;
                                    title = widget.stations[index].name!;
                                    print(path);
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 8,),
                        ],
                      );
                }else return Container();



                }),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.grey,
                textStyle: const TextStyle(color: Colors.white),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0))),
            onPressed: () {
              if(path == -1){
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) =>
                      MessageDialog(message: "Veuillez sélectionner une route!"),
                );
                return;
              }
         /*     Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Trajets(routes:path)));*/
            },
            child: SizedBox(
              height: 0.06.sh,
              width: 0.8.sw,
              child: Center(
                child: Text(
                  "Suivant",
                  style: TextStyle(
                      fontSize: 20.0.sp,
                      fontFamily: 'Jost',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
