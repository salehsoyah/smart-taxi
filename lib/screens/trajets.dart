import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/models/route.dart';
import 'package:taxi_finder/models/station.dart';
import 'package:taxi_finder/screens/stations_depart.dart';

import '../assistants/assistant_methods.dart';
import '../data/app_data.dart';
import '../widgets/message_dialog.dart';
class Trajets extends StatefulWidget {
  Trajets({Key? key, required this.routes}) : super(key: key);
  static const String idScreen = "pathScreen";
  List<RouteItem> routes;

  @override
  _TrajetsState createState() => _TrajetsState();
}

class _TrajetsState extends State<Trajets> {
  int path = -1;
  String? title = 'Route de Sidi Mansour';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffB9B0A2),
      appBar: AppBar(
        toolbarHeight: 0.08.sh,
        backgroundColor: Color(0xffB9B0A2),
        title: Image.asset("assets/images/logo_2.png"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding:
            EdgeInsets.only(left: 0.02.sw, bottom: 0.009.sh, top: 0.01.sh),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Choisissez un Tajet",
                 style:TextStyle(
                    fontSize: 25.sp,
                    fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: widget.routes.length,
                itemBuilder: (BuildContext context, int index) {
                  return
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 26.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color:Colors.grey,
                              ),

                              child: RadioListTile(
                                activeColor: Color(0xffB9B0A2),
                                title: Text(
                                  widget.routes[index].name!,
                                  style: TextStyle(
                                      fontFamily: 'Jost',
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.sp),
                                ),
                                value: index,
                                groupValue: path,
                                onChanged: (int? value) {
                                  setState(() {
                                    path = value!;
                                    title = widget.routes[index].name!;
                                    print(path);
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 8,),
                        ],
                      );



                }),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.grey,
                textStyle: const TextStyle(color: Colors.white),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0))),
            onPressed: () {
            /*  if(path == -1){
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) =>
                      MessageDialog(message: "Veuillez sélectionner une route!"),
                );
                return;
              }
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MainScreen(
                        title: title,
                        homePage:  HomeTabPagePath(idRoute: path,),
                      )));*/
              Provider.of<AppData>(context,listen: false).updateRoute(newRoute: path);
             print("select route id : ${Provider.of<AppData>(context,listen: false).updateRoute(newRoute: path)}");

               if(path == -1){
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) =>
                      MessageDialog(message: "Veuillez sélectionner une route!"),
                );
                return;
              }
               print(path);
               AssistantMethods.obtainStationOfRoutes(path).then((value) {
                 Navigator.push(
                     context,
                     MaterialPageRoute(
                         builder: (context) => Stations(stations: value!)));
               });
            },
            child: SizedBox(
              height: 0.06.sh,
              width: 0.8.sw,
              child: Center(
                child: Text(
                  "Suivant",
                  style: TextStyle(
                      fontSize: 20.0.sp,
                      fontFamily: 'Jost',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
