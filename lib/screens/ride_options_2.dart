import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/assistants/assistant_methods.dart';
import 'package:taxi_finder/assistants/geo_fire_assistant.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/screens/main_screen.dart';

class RideOptions extends StatefulWidget {
  static const String idScreen = "optionsScreen";

  const RideOptions({Key? key}) : super(key: key);

  @override
  State<RideOptions> createState() => _RideOptionsState();
}

class _RideOptionsState extends State<RideOptions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Image.asset(
                  "assets/images/background/rideoptionbackground.png",
                  fit: BoxFit.fill),
            ),
          ),
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 0.20.sw,
                  child: Image.asset(
                    "assets/images/logo/logoinno.png",
                  ),
                ),
                SizedBox(height: 0.10.sw),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Image.asset('assets/images/rdiobtn2.png'),
                      iconSize: 0.230.sh,
                      onPressed: () {
                        setState(() {
                          GeoFireAssistant.nearByAvailableDriversList.clear();
                        });

                        Provider.of<AppData>(context, listen: false).updateAppType(1);

                        AssistantMethods.obtainRoutes().then(
                              (value) {
                            Provider.of<AppData>(context, listen: false)
                                .updateStationsList(value!);
                            print(Provider.of<AppData>(context, listen: false)
                                .stations
                                .toString() +
                                ' fzzfgggg');
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MainScreen(
                                  type: '1',
                                ),
                              ),
                            );
                          },
                        );

                      },
                    ),

                    IconButton(
                      icon: Image.asset('assets/images/rdiobtn3.png'),
                      iconSize: 0.230.sh,
                      onPressed: () {
                        setState(() {
                          GeoFireAssistant.nearByAvailableDriversList.clear();
                        });
                        Provider.of<AppData>(context, listen: false).updateAppType(2);

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainScreen(
                                  type: '2',
                                )));
                      },
                    ),
                  ],
                ),
                SizedBox(height: 0.05.sw),
               /* IconButton(
                  icon: Image.asset('assets/images/rdiobtn1.png'),
                  iconSize: 0.230.sh,
                  onPressed: () {},
                ),*/
              ],
            ),
          ),
        ],
      ),
    );
  }
}
