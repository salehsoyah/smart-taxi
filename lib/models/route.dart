class RouteItem {
  int? id;
  String? name;

  RouteItem({this.id, this.name});

  factory RouteItem.fromJson(Map<String, dynamic> json) => RouteItem(
    id: json["id"] ?? 0,
    name: json["name"] ?? '',
  );
}