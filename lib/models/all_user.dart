import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class Users {
  int? id;
  String? email;
  String? name;
  String? phone;
  String? firstName;
  String? cin;
  String? address;
  String? photo;

  Users(
      {this.id,
      this.email,
      this.name,
      this.phone,
      this.firstName,
      this.cin,
      this.address,
      this.photo});

  factory Users.fromJson(Map<String, dynamic> json) => Users(
        id: json["id"] ?? 0,
        email: json['email'] ?? '',
        name: json["nom"] ?? '',
        phone: json["num_tel"] ?? '',
        firstName: json["prenom"] ?? '',
        cin: json["cin"] ?? '',
        address: json["adresse"] ?? '',
        photo: json["photo"] ?? '',
      );
}
