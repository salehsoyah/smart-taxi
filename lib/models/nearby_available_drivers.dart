class NearbyAvailableDrivers {
  String? key;
  double? latitude;
  double? longitude;
  int? time;
  int? availablePlaces;
  int? direction;
  int? route;

  NearbyAvailableDrivers(
      {this.key,
      this.latitude,
      this.longitude,
      this.time,
      this.availablePlaces,
      this.direction,
      this.route});
}
