class Address {
  String placeFormattedAddress;
  String placeName;
  String placeId;
  double latitude;
  double longitude;
  String? path;

  Address(
      {this.placeFormattedAddress = '',
      this.placeName = '',
      this.placeId = '',
      this.latitude = 0,
      this.longitude = 0,
      this.path = ''});
}
