import 'package:flutter/material.dart';

class User {
  String? id;
  String? firstName;
  String? familyName;
  String? phone;
  String? cin;
  String? address;
  String? email;
  String? password;
  String? photo;

  User(
      {this.id,
      this.firstName,
      this.familyName,
      this.phone,
      this.cin,
      this.address,
      this.email,
      this.password,
      this.photo});

  Map<String, dynamic> toJson() => {
        'nom': familyName,
        'prenom': firstName,
        'num_tel': phone,
        'password': password,
        'email': email,
        'cin': cin,
        'adresse': address,
        'photo': photo
      };
}
