class Station {
  int? idRoute;
  String? name;
  String? routeName;
  int? order;
  double? latitude;
  double? longitude;

  Station(
      {this.idRoute,
      this.name,
      this.routeName,
      this.order,
      this.latitude,
      this.longitude});

  factory Station.fromJson(Map<String, dynamic> json) => Station(
        idRoute: json["id_voix"] ?? 0,
        name: json['station'] ?? '',
        routeName: json['voix'] ?? '',
        order: json["ordre"] ?? 0,
        latitude: json['latt_depart'] != null
            ? double.parse(json['latt_depart'])
            : 0.0,
        longitude:
            json['lng_depart'] != null ? double.parse(json['lng_depart']) : 0.0,
      );
}
