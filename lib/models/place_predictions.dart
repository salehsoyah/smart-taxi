class PlacePrediction {
  String secondaryText;
  String mainText;
  String placeId;
  int type;

  factory PlacePrediction.fromJson(Map<String, dynamic> json, int type) =>
      PlacePrediction(
          placeId: json["place_id"] != null ? json['place_id'] : ' ',
          mainText: json["structured_formatting"]["main_text"] ?? ' ',
          secondaryText: json["structured_formatting"]["secondary_text"] ?? ' ',
          type: type);

  PlacePrediction(
      {this.secondaryText = ' ',
      this.mainText = ' ',
      this.placeId = ' ',
      this.type = 1});

  Map<String, dynamic> toJson() => {
    'secondaryText': secondaryText,
    'mainText': mainText,
    'placeId': placeId,
    'type': type,
  };
}
