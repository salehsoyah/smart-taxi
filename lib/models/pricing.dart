class Pricing {
  int? id;
  double? kmPrice;
  double? timePrice;
  double? coef;
  String? hour;
  String? day;

  Pricing({this.id, this.kmPrice, this.hour, this.day, this.timePrice, this.coef});

  factory Pricing.fromJson(Map<String, dynamic> json) => Pricing(
        id: json["id"] ?? 0,
        kmPrice: json['prix_km'] != null ? double.parse(json['prix_km']) : 0.0,
        hour: json["caracteristique_heure"] ?? '',
        day: json["caracteristique_jour"] ?? '',
        timePrice:
            json['prix_temps'] != null ? double.parse(json['prix_temps']) : 0.0,
        coef: json['coef'] != null ? double.parse(json['coef']) : 0.0,
      );
}
