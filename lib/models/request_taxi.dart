class RequestTaxi {
  int ? idTaxi;
  int? idClient;
  String? location;
  String? destination;
  String? pricing;
  String? status;
  String? date;

  RequestTaxi(
      {this.idTaxi,
      required this.idClient,
      required this.location,
      required this.destination,
      required this.pricing,
      required this.status,
      required this.date});

  factory RequestTaxi.fromJson(Map<String, dynamic> json) => RequestTaxi(
    idTaxi: json["id_taxi"] ?? 0,
    idClient: json['id_client'] ?? 0,
    location: json["nom_depart"] ?? '',
    destination: json["nom_arrivee"] ?? '',
    pricing: json["tarif"] ?? '',
    status: json["status"] ?? '',
    date: json["created_at"] ?? '',
  );

  Map<String, dynamic> toJson() => {
        'id_taxi': idTaxi,
        'id_client': idClient,
        'depart': location,
        'arrive': destination,
        'tarif': pricing,
        'status': status,
      };
}
