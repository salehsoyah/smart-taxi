import 'dart:async';
import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:taxi_finder/models/place_predictions.dart';

class LocationHelper {
  LocationHelper._privateConstructor();

  static final LocationHelper instance = LocationHelper._privateConstructor();
  static Database? _database;

  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory placesDirectory = await getApplicationDocumentsDirectory();
    String path = join(placesDirectory.path, 'places.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE places (id INTEGER PRIMARY KEY, secondaryText TEXT, mainText TEXT, placeId TEXT, type INTEGER)');
  }

  Future<List<dynamic>> getPlaces() async {
    Database db = await instance.database;
    var places = await db.query('places', orderBy: 'secondaryText');

    return places;
  }

  Future<int> add(PlacePrediction placePrediction) async {
    Database db = await instance.database;
    var queryResult = await db.rawQuery(
        'SELECT * FROM places WHERE placeId="${placePrediction.placeId}"');
    if(queryResult.isEmpty){
      return await db.insert('places', placePrediction.toJson());
    }
    return 0;
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('places', where: 'id = ?', whereArgs: [id]);
  }
}
