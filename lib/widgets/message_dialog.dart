import 'package:flutter/material.dart';

class MessageDialog extends StatelessWidget {
  String message;

  MessageDialog({Key? key, this.message = ""}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.grey,
      child: Container(
        margin: const EdgeInsets.all(7.0),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(
                  children: [
                    const SizedBox(width: 6.0),
                    const Icon(Icons.error),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: Text(
                        message,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 13.0,
                            fontFamily: 'Jost',
                            fontWeight: FontWeight.bold,
                            overflow: TextOverflow.ellipsis),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.close)),
            ],
          ),
        ),
      ),
    );
  }
}
