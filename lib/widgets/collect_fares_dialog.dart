import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:taxi_finder/assistants/geo_fire_assistant.dart';

class CollectFaresDialog extends StatelessWidget {
  const CollectFaresDialog(
      {Key? key, required this.paymentMethod, required this.fareAmount})
      : super(key: key);
  final String paymentMethod;
  final double fareAmount;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      backgroundColor: Colors.orange,
      child: Container(
        margin: EdgeInsets.all(7.0.sp),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
             SizedBox(
              height: 18.sp,
            ),
             Text(
              "Payment en espèce",
              style: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.bold, fontSize: 18.sp),
            ),
            const Divider(
              height: 2.0,
              thickness: 2.0,
            ),
            const SizedBox(
              height: 16.0,
            ),
            Text(
              "TND $fareAmount",
              style:  TextStyle(
                  fontSize: 50.sp,
                  fontFamily: 'Jost',
                  fontWeight: FontWeight.bold),
            ),
             SizedBox(
              height: 12.0.sp,
            ),
             Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0.sp),
              child: Text(
                "C'est le montant total du voyage, il a été facturé au passager",
                style:
                    TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.bold, fontSize: 12.sp),
                textAlign: TextAlign.center,
              ),
            ),
             SizedBox(height: 30.0.sp),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: 16.0.sp),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.orange),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0),
                    ),
                  ),
                ),
                onPressed: () async {
                  GeoFireAssistant.nearByAvailableDriversList.clear();

                  Navigator.pop(context, 'close');
                },
                child: Padding(
                  padding:  EdgeInsets.all(13.0.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:  [
                      Text(
                        "Payer",
                        style: TextStyle(
                            fontSize: 20.0.sp,
                            fontFamily: "Jost",
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Icon(
                        FontAwesomeIcons.cashRegister,
                        color: Colors.white,
                        size: 22.0.sp,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30.0,
            )
          ],
        ),
      ),
    );
  }
}
