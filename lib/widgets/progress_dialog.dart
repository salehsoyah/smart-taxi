import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProgressDialog extends StatelessWidget {
  String message;

  ProgressDialog({Key? key, this.message = ""}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.orange,
      child: Container(
        margin:  EdgeInsets.fromLTRB(0.015.sw, 0.015.sw, 0.015.sw, 0.015.sw),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Padding(
          padding:  EdgeInsets.fromLTRB(0.025.sw, 0.025.sw, 0.025.sw, 0.025.sw),
          child: Row(
            children: [
               SizedBox(
                width: 6.0.sp,
              ),
              const CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
              ),
              const SizedBox(
                width: 26.0,
              ),
              Expanded(
                child: Text(
                  message,
                  style:  TextStyle(
                      color: Colors.black,
                      fontSize: 13.0.sp,
                      fontFamily: 'Jost',
                      fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
