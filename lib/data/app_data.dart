import 'package:flutter/cupertino.dart';
import 'package:taxi_finder/models/address.dart';
import 'package:taxi_finder/models/station.dart';

class AppData extends ChangeNotifier {
  Address? pickUpLocation,
      dropOffLocation,
      closestPointPickUp,
      closestPointDropOff;
  double? fares;
  double? driverTime = 0;
  int? appType;
  String placeFee = '0';
  List<Station> stations = [];
  Station? pickUpStation;
  Station? dropOffStation;
  int routeid =0 ;

  void updatePlaceFee(String price) {
    placeFee = price;
    notifyListeners();
  }

  void updateAppType(int type) {
    appType = type;
    notifyListeners();
  }

  updateDriverTime(double time) {
    driverTime = time;
    notifyListeners();
  }

  updateFares(double newFares) {
    fares = newFares;
    notifyListeners();
  }
  updateRoute({int? newRoute}){
    routeid=newRoute! ;
    notifyListeners() ;
    return routeid ;
  }
  getRoute(){

    notifyListeners() ;
    return routeid ;
  }

  void updatePickUpLocationAddress(Address pickUpAddress) {
    pickUpLocation = pickUpAddress;
    notifyListeners();
  }

  void updateClosestPickUpLocationAddress(Address pickUpAddress, Station station) {
    pickUpStation = station;
    closestPointPickUp = pickUpAddress;
    notifyListeners();
  }

  void updateDropOffLocationAddress(Address dropOffAddress) {
    dropOffLocation = dropOffAddress;
    notifyListeners();
  }

  void updateClosestDropOffLocationAddress(Address dropOffAddress, Station station) {
    dropOffStation = station;
    closestPointDropOff = dropOffAddress;
    notifyListeners();
  }

  void updateStationsList(List<Station> newStations) {
    stations = newStations;
    notifyListeners();
  }
}
