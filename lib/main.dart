import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/screens/login_screen.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/screens/register_screen.dart';
import 'package:taxi_finder/screens/ride_options.dart';
import 'package:taxi_finder/screens/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  currentFirebaseUser = FirebaseAuth.instance.currentUser;

  SharedPreferences preferences = await SharedPreferences.getInstance();
  var loggedIn = preferences.getBool('loggedIn') ?? false;

  runApp(MyApp(
    loggedIn: loggedIn,
  ));
}

DatabaseReference userRef =
FirebaseDatabase.instance.reference().child("users");
DatabaseReference rideRequestRef =
FirebaseDatabase.instance.reference().child("Ride Requests");
DatabaseReference driverRef =
FirebaseDatabase.instance.reference().child("drivers");

class MyApp extends StatelessWidget {
  MyApp({Key? key, required this.loggedIn}) : super(key: key);

  bool loggedIn;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => AppData(),
        child: ScreenUtilInit(
          designSize: const Size(360, 690),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (BuildContext context, Widget? child) => MaterialApp(
            title: 'Taxi Finder App',
            theme: ThemeData(
              primarySwatch: Colors.blue,
              fontFamily: "Jost",
            ),
            debugShowCheckedModeBanner: false,
            initialRoute:
            loggedIn ? RideOptions.idScreen: LoginScreen.idScreen,
            routes: {
              RegisterScreen.idScreen: (context) => RegisterScreen(),
              LoginScreen.idScreen: (context) => LoginScreen(),
              SplashScreen.idScreen: (context) => const SplashScreen(),
              RideOptions.idScreen: (context) => const RideOptions(),

            },
          ),
        ));
  }
}
