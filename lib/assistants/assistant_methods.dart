import 'dart:convert';
import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/assistants/request_assistant.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/models/address.dart';
import 'package:taxi_finder/models/all_user.dart';
import 'package:taxi_finder/models/direction_details.dart';
import 'package:taxi_finder/models/pricing.dart';
import 'package:taxi_finder/models/request_taxi.dart';
import 'package:http/http.dart' as http;
import 'package:taxi_finder/models/route.dart';
import 'package:taxi_finder/models/station.dart';

class AssistantMethods {
  static Future<String> searchCoordinateAddress(
      Position position, context) async {
    String placeAddress = "";
    String st1, st2, st3, st4;
    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$mapKey2";
    var response = await RequestAssistant.getRequest(Uri.parse(url));
    if (response != "Failed.") {
      placeAddress = response["results"][0]["formatted_address"];

      // st1 = response["results"][0]["address_components"][3]["long_name"];
      // st2 = response["results"][0]["address_components"][4]["long_name"];
      // // st3 = response["results"][0]["address_components"][5]["long_name"];
      // st4 = response["results"][0]["address_components"][6]["long_name"];
      // placeAddress = st1 + ", " + st2 + ", " + st4;

      Address userPickedAddress = Address();
      userPickedAddress.longitude = position.longitude;
      userPickedAddress.latitude = position.latitude;
      userPickedAddress.placeName = placeAddress;

      Provider.of<AppData>(context, listen: false)
          .updatePickUpLocationAddress(userPickedAddress);
    }

    return placeAddress;
  }

  static Future<DirectionDetails?> obtainDirectionDetails(
      LatLng initialPosition, LatLng finalPosition) async {
    String directionUrl =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${initialPosition.latitude},${initialPosition.longitude}&destination=${finalPosition.latitude},${finalPosition.longitude}&key=$mapKey";
    var res = await RequestAssistant.getRequest(Uri.parse(directionUrl));
    if (res == 'Failed.') {
      return null;
    }
    DirectionDetails directionDetails = DirectionDetails();

    directionDetails.encodedPoints =
        res['routes'][0]['overview_polyline']['points'];
    directionDetails.distanceText =
        res['routes'][0]['legs'][0]['distance']['text'];
    directionDetails.distanceValue =
        res['routes'][0]['legs'][0]['distance']['value'];
    directionDetails.durationText =
        res['routes'][0]['legs'][0]['duration']['text'];
    directionDetails.durationValue =
        res['routes'][0]['legs'][0]['duration']['value'];

    return directionDetails;
  }

  static Future<Pricing?> obtainRoutePricing() async {
    String pricingUrl = "https://smartcity.proxiweb.tn/api/tarifKM/findbyday";
    var res = await RequestAssistant.getRequest(Uri.parse(pricingUrl));
    if (res == 'Failed.') {
      return null;
    }

    var pricing = Pricing.fromJson(res['data'][0]);
    return pricing;
  }

  static double calculateFares(
      DirectionDetails directionDetails,
      double driverDistance,
      int driverTime,
      double pricingTime,
      double pricingKm,
      double coef,
      BuildContext context) {
    // print("driverTime ${driverTime / 60}");
    // print("driverDistance $driverDistance");
    // print("destinaTime ${directionDetails.durationValue! / 60}");
    // print("destinaDistance ${directionDetails.distanceValue}");
    // print("pricingTime $pricingTime");
    // print("pricingKm $pricingKm");
    // print("coef $coef");

    //tnd
    double distanceTraveledFare =
        ((((directionDetails.distanceValue! / 1000) + driverDistance) *
                    pricingKm) +
                (((directionDetails.durationValue! / 60) + (driverTime / 60)) *
                    pricingTime) +
                0.54) *
            coef;
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Provider.of<AppData>(context, listen: false)
          .updateFares(double.parse(distanceTraveledFare.toStringAsFixed(2)));

      Provider.of<AppData>(context, listen: false)
          .updateDriverTime(driverTime / 60);
    });

    return distanceTraveledFare;
  }

  static void getCurrentOnlineUserInfo() async {
    firebaseUser = FirebaseAuth.instance.currentUser;
    String userId = firebaseUser!.uid;
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("users").child(userId);
  }

  static void sendRequestTaxi(RequestTaxi requestTaxi) async {
    String url = "http://192.168.1.48:8000/api/reservationkm/save";
    var res = await RequestAssistant.postRequest(Uri.parse(url), requestTaxi);
    if (res == 'Failed.') {
      print("failed");
    } else {
      print("selket");
    }
  }

  static double createRandomNumber(int num) {
    var random = Random();
    int radNumber = random.nextInt(num);
    return radNumber.toDouble();
  }

  static sendNotificationToDriver(
      String token, context, String rideRequestId) async {
    var destination =
        Provider.of<AppData>(context, listen: false).dropOffLocation;

    Map<String, String> headerMap = {
      'content-type': 'application/json',
      'Authorization': serverKey
    };
    Map notificationMap = {
      'body': 'Destination , ${destination!.placeName}',
      'title': 'Nouvelle demande'
    };
    Map dataMap = {
      'click_action': 'FLUTTER_NOTIFICATION_CLICK',
      'id': '1',
      'status': 'done',
      'ride_request_id': rideRequestId
    };
    Map sendNotificationsMap = {
      "notification": notificationMap,
      "data": dataMap,
      "priority": "high",
      "to": token
    };
    var res = await http
        .post(Uri.parse("https://fcm.googleapis.com/fcm/send"),
            headers: headerMap, body: jsonEncode(sendNotificationsMap))
        .then((value) {
      print(value.body);
    });
  }

  static Future<List<Station>?> obtainRoutes() async {
    String url = "https://smartcity.proxiweb.tn/api/segment/getstation";
    var res = await RequestAssistant.getRequest(Uri.parse(url));
    if (res == 'Failed.') {
      return null;
    }

    final List routes = res['data'];
    return routes.map((e) => Station.fromJson(e)).toList();
  }
  static Future<List<RouteItem>?> obtainRoutess() async {
    String routesUrl = "https://smartcity.proxiweb.tn/api/trajets/get";
    var res = await RequestAssistant.getRequest(Uri.parse(routesUrl));
    if (res == 'Failed.') {
      return null;
    }    print("routes " + res.toString());

    final List routes = res['data'];
    return routes.map((e) => RouteItem.fromJson(e)).toList();
  }

  static Future<List<Station>?> obtainStationOfRoutes(int trajet_id) async {
    String routesUrl = "https://smartcity.proxiweb.tn/api/segment/getbytrajet/${trajet_id}";
    var res = await RequestAssistant.getRequest(Uri.parse(routesUrl));
    if (res == 'Failed.') {
      return null;
    }    print("StatioOfroutes " + res.toString());

    final List routes = res['data'];
    return routes.map((e) => Station.fromJson(e)).toList();
  }


  Future<String?> obtainPricePlace(
      int idRoute, orderPickUp, orderDropOff) async {
    String url =
        "https://smartcity.proxiweb.tn/api/tarifVoix/getTarifByCriteria/$idRoute/$orderPickUp/$orderDropOff";
    var res = await RequestAssistant.getRequest(Uri.parse(url));
    if (res == 'Failed.') {
      return null;
    }
    print(idRoute.toString() + '  idvoix');
    print(orderPickUp.toString() + '  order1');
    print(orderDropOff.toString() + '  order2');

    print(res['data'].toString() + '  fzcccczaawwc');
    return res['data'].toString();
  }
}
