import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:taxi_finder/data/app_data.dart';
import 'package:taxi_finder/models/nearby_available_drivers.dart';
import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:taxi_finder/models/station.dart';

class GeoFireAssistant {
  static List<NearbyAvailableDrivers> nearByAvailableDriversList = [];

  static void removeDriversFromList(String key) {
    nearByAvailableDriversList.removeDuplicates(by: ((item) => item.key));
    int index =
        nearByAvailableDriversList.indexWhere((element) => element.key == key);
    if (nearByAvailableDriversList.isNotEmpty) {
      nearByAvailableDriversList.removeAt(index);
    }
  }

  static void updateDriverNearbyLocation(NearbyAvailableDrivers driver) {
    nearByAvailableDriversList.removeDuplicates(by: ((item) => item.key));
    int index = nearByAvailableDriversList
        .indexWhere((element) => element.key == driver.key);
    nearByAvailableDriversList[index].latitude = driver.latitude;
    nearByAvailableDriversList[index].longitude = driver.longitude;
  }

  static List<NearbyAvailableDrivers> getDriversByOptions(BuildContext context) {
    var providerData = Provider.of<AppData>(context, listen: false);
    var directionValue = providerData.pickUpStation!.order! - providerData.dropOffStation!.order!;
    int direction = directionValue > 0 ? 0 : 1;
    int route = providerData.pickUpStation!.idRoute!;

    return nearByAvailableDriversList
        .where((element) =>
            element.direction == direction && element.route == route)
        .toList();
  }
}
