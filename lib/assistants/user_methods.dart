import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxi_finder/assistants/request_assistant.dart';
import 'package:taxi_finder/config_maps.dart';
import 'package:http/http.dart' as http;
import 'package:taxi_finder/models/all_user.dart';
import 'package:taxi_finder/models/request_taxi.dart';
import 'package:taxi_finder/models/user.dart';
import 'package:taxi_finder/screens/login_screen.dart';
import 'package:taxi_finder/screens/main_screen.dart';
import 'package:taxi_finder/screens/ride_options.dart';
import 'package:taxi_finder/widgets/message_dialog.dart';
import 'package:taxi_finder/widgets/progress_dialog.dart';

import '../screens/code_screen.dart';

class UserMethods {
  static void registerUser(User? user, context) async {
    String url = "https://smartcity.proxiweb.tn/api/clients/save";
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) =>
          ProgressDialog(message: "Inscription, veuillez patienter..."),
    );
    Response response = await http.post(Uri.parse(url),
        // Send authorization headers to the backend.
        headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
        body: user!.toJson());
    Navigator.pop(context);
    var body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      if (body["data"]["code"] == 200) {
        print(body);
        SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
        sharedPreferences.setBool("loggedIn", true);
        sharedPreferences.setInt("userId", body["data"]['client']['id']);
        sharedPreferences.setString(
            'userName',
            body["data"]['client']['prenom'] +
                '' +
                body["data"]['client']['nom']);
        sharedPreferences.setString(
            'userPhone',
            body["data"]['client']['num_tel']);

        Navigator.pushNamedAndRemoveUntil(
            context, RideOptions.idScreen, (route) => false);
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) =>
              MessageDialog(message: body["data"]["message"]),
        );
      }
    }
  }
  static void loginUserByPhone(String numtel, context) async {
    String url = "https://smartcity.proxiweb.tn/api/clients/send_sms";

    var body = {"num_tel": numtel};

    Response response = await http.post(
      Uri.parse(url),
      body: body,
    );
    var res = jsonDecode(response.body);
    print(res);
    if (response.statusCode == 200) {
      if (res["data"]["code"] == 200) {
        SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
        //sharedPreferences.setBool("loggedIn", true);
        sharedPreferences.setString(
          'userPhone',
          res["data"]['client']['num_tel'],
        );
        sharedPreferences.setInt(
          'codesms',
          res["data"]['client']['code_sms'],
        );
        Future.delayed(
          const Duration(
            seconds: 2,
          ),
              () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => const CodeScreen(),
              ),
            );
          },
        );
      } else if (res["data"]["code"] == 400) {
        SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
        //sharedPreferences.setBool("loggedIn", true);
        sharedPreferences.setString(
          'userPhone',
          res["data"]['client'][0]['num_tel'],
        );
        sharedPreferences.setInt(
          'codesms',
          res["data"]['client'][0]['code_sms'],
        );
        Future.delayed(
          const Duration(
            seconds: 1,
          ),
              () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => const CodeScreen(),
              ),
            );
          },
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) =>
              MessageDialog(message: res["data"]["message"]),
        );
      }
    } else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
            MessageDialog(message: res["data"]["message"]),
      );
    }
  }
  static void verifierCode(String code, context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String? phone = sharedPreferences.getString('userPhone');
    String? codeExiste = sharedPreferences.getInt('codesms').toString();

    if (codeExiste == code) {
      print('succes');
      Future.delayed(
        const Duration(seconds: 2),
            () {
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => const RideOptions(),
            ),
                (Route<dynamic> route) => false,
          );
        },
      );
      sharedPreferences.setBool("loggedIn", true);
    } else {
      print('Failed.');
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
            MessageDialog(message: "Code entrer incorrect"),
      );
    }
  }


  static void loginUser(String email, String password, context) async {
    String url = "https://smartcity.proxiweb.tn/api/clients/login";

    var queryParameters = {
      'login': email,
      'password': password,
    };

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) =>
          ProgressDialog(message: "Inscription, veuillez patienter..."),
    );
    Response response = await http.post(Uri.parse(url),
        // Send authorization headers to the backend.
        headers: {HttpHeaders.authorizationHeader: "x-www-form-urlencoded"},
        body: queryParameters);
    Navigator.pop(context);
    var body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      if (body["data"]["code"] == 200) {

        SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
        sharedPreferences.setBool("loggedIn", true);
        sharedPreferences.setInt("userId", body["data"]['client'][0]['id']);
        sharedPreferences.setString(
            'userName',
            body["data"]['client'][0]['prenom'] +
                ' ' +
                body["data"]['client'][0]['nom']);
        sharedPreferences.setString(
            'userPhone',
            body["data"]['client'][0]['num_tel']);
        sharedPreferences.setInt(
            'userId',
            body["data"]['client'][0]['id']);
        Navigator.pushNamedAndRemoveUntil(
            context, RideOptions.idScreen, (route) => false);
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) =>
              MessageDialog(message: body["data"]["message"]),
        );
      }
    }
  }

  static Future<Users?> obtainProfileData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    int? id = sharedPreferences.getInt('userId');
    String profileUrl = "https://smartcity.proxiweb.tn/api/clients/findbyid/$id";
    var res = await RequestAssistant.getRequest(Uri.parse(profileUrl));
    if (res == 'Failed.') {
      return null;
    }

    var user = Users.fromJson(res['data'][0]);
    return user;
  }

  static Future<List<RequestTaxi>?> obtainReservations() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    int? id = sharedPreferences.getInt('userId');
    String url = "https://smartcity.proxiweb.tn/api/reservationkm/getbyclient/$id";
    var res = await RequestAssistant.getRequest(Uri.parse(url));
    if (res == 'Failed.') {
      return null;
    }

    final List reservations = res['data'];
    return reservations.map((e) => RequestTaxi.fromJson(e)).toList();
  }

}
