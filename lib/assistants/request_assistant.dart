import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:taxi_finder/models/request_taxi.dart';

class RequestAssistant{
  static Future<dynamic> getRequest(Uri url) async{
    http.Response response = await http.get(url);

    try{
      if(response.statusCode == 200){
        String jsonData = response.body;
        var decodeData = jsonDecode(jsonData);
        return decodeData;
      }
      else{
        return "Failed.";
      }
    }catch(exp){
      return "Failed.";
    }
  }

  static Future<dynamic> postRequest(Uri url, RequestTaxi requestTaxi) async{

    http.Response response = await http.post(url, body: requestTaxi.toJson());
    print("here3");

    try{
      if(response.statusCode == 200){
        print("hello");
      }
      else{
        return "Failed.";
      }
    }catch(exp){
      return "Failed.";
    }
  }
}